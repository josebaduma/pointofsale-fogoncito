(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Express.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Express.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Express",
  data: function data() {
    return {
      listOrdenes: []
    };
  },
  mounted: function mounted() {
    this.listOrdenesService();
  },
  methods: {
    facturar: function facturar(id) {
      var data = this.listOrdenes.find(function (element) {
        return element.ord_id == id;
      });
      var listCarrito = [];
      data.pedidos.forEach(function (item) {
        var itemcar = {
          nombre: item.prod_name,
          cant: item.detalle_orden_cantidad,
          precio: item.prod_price
        };
        listCarrito.push(itemcar);
      });
      var formData = {
        pedidos: JSON.stringify(listCarrito),
        mesa: data.ord_mesa,
        cuenta: data.ord_id,
        tipo: data.ord_tipo,
        empleado: data.ord_empleado,
        cedula: data.cliente_cedula,
        cliente: data.cliente_nombre,
        telefono: data.cliente_telefono,
        subtotal: data.ord_subtotal,
        impuesto: data.ord_impuesto,
        total: data.ord_total
      };
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("ventas/print", formData).then(function (response) {})["catch"](function (error) {
        console.log(error);
      });
    },
    delete_orden: function delete_orden(id) {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("orden/delete/" + id).then(function (response) {
        _this.listOrdenesService();
      })["catch"](function (error) {
        alert(error);
      });
    },
    convertMoney: function convertMoney(value) {
      var formatterPeso = new Intl.NumberFormat("es-CR", {
        style: "currency",
        currency: "CRC",
        minimumFractionDigits: 0
      });
      var valueFinal = formatterPeso.format(value);
      return valueFinal;
    },
    listOrdenesService: function listOrdenesService() {
      var _this2 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("orden/list/express").then(function (response) {
        _this2.listOrdenes = response.data;
      })["catch"](function (error) {
        alert(error);
      });
    }
  },
  computed: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Express.vue?vue&type=template&id=258eeba1&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Express.vue?vue&type=template&id=258eeba1& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container main-pos" }, [
    _c("br"),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12 order-md-1" }, [
        _c(
          "h4",
          { staticClass: "mb-3" },
          [
            _c("span", [_vm._v("Ordenes Express")]),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "btn btn-outline-info mx-2",
                attrs: { to: { name: "Orden-Express", params: { id: 0 } } }
              },
              [_vm._v("Agregar Orden")]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12 order-md-1" }, [
            _c("hr"),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "row" },
              _vm._l(_vm.listOrdenes, function(data, index) {
                return _c(
                  "div",
                  {
                    key: index,
                    staticClass:
                      "mesas card col-md-2 border alert alert-warning"
                  },
                  [
                    _c(
                      "div",
                      { staticClass: "card-body" },
                      [
                        _c("h5", { staticClass: "card-title" }, [
                          _c("span", [
                            _vm._v("Orden Express " + _vm._s(data.ord_id))
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "router-link",
                          {
                            staticClass:
                              "btn btn-outline-dark btn-sm btn-block",
                            staticStyle: { "margin-top": "15px" },
                            attrs: {
                              to: {
                                name: "Orden-Express",
                                params: { id: data.ord_id }
                              }
                            }
                          },
                          [_vm._v("Modificar")]
                        )
                      ],
                      1
                    )
                  ]
                )
              }),
              0
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Express.vue":
/*!****************************************!*\
  !*** ./resources/js/views/Express.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Express_vue_vue_type_template_id_258eeba1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Express.vue?vue&type=template&id=258eeba1& */ "./resources/js/views/Express.vue?vue&type=template&id=258eeba1&");
/* harmony import */ var _Express_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Express.vue?vue&type=script&lang=js& */ "./resources/js/views/Express.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Express_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Express_vue_vue_type_template_id_258eeba1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Express_vue_vue_type_template_id_258eeba1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Express.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Express.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./resources/js/views/Express.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Express_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Express.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Express.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Express_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Express.vue?vue&type=template&id=258eeba1&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/Express.vue?vue&type=template&id=258eeba1& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Express_vue_vue_type_template_id_258eeba1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Express.vue?vue&type=template&id=258eeba1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Express.vue?vue&type=template&id=258eeba1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Express_vue_vue_type_template_id_258eeba1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Express_vue_vue_type_template_id_258eeba1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);