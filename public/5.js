(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Categorias.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Categorias.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Categorias",
  data: function data() {
    return {
      categoria: {
        id: 0,
        nombre: "",
        cocina: ""
      },
      editar: false,
      currentSort: "name",
      currentSortDir: "asc",
      pageSize: 6,
      currentPage: 1
    };
  },
  mounted: function mounted() {
    this.getCategorias();
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])(["getCategorias"])), {}, {
    nextPage: function nextPage() {
      if (this.currentPage * this.pageSize < this.listaCategorias.length) this.currentPage++;
    },
    prevPage: function prevPage() {
      if (this.currentPage > 1) this.currentPage--;
    },
    sort: function sort(s) {
      if (s === this.currentSort) {
        this.currentSortDir = this.currentSortDir === "asc" ? "desc" : "asc";
      }

      this.currentSort = s;
    },
    addCategory: function addCategory() {
      var _this = this;

      var formData = new FormData();
      formData.append("name", this.categoria.nombre);
      formData.append("cocina", this.categoria.cocina);
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("category/create", formData).then(function (response) {
        _this.categoria.nombre = "";
        _this.categoria.cocina = "";

        _this.getCategorias();
      })["catch"](function (error) {
        alert(error);
      });
    },
    editCategory: function editCategory() {
      var _this2 = this;

      var formData = {
        idcat: this.categoria.id,
        name: this.categoria.nombre,
        cocina: this.categoria.cocina
      };
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("category/update", formData).then(function (response) {
        _this2.categoria.nombre = "";
        _this2.categoria.cocina = "";
        _this2.editar = false;
        alert(response.data.message);

        _this2.getCategorias();
      })["catch"](function (error) {
        alert(error);
      });
    },
    setVal: function setVal(val_id, val_name, val_cocina) {
      this.categoria.id = val_id;
      this.categoria.nombre = val_name;
      this.categoria.cocina = val_cocina;
      this.editar = true;
    },
    deleteCategoria: function deleteCategoria(val_id) {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("category/delete/" + val_id).then(function (response) {
        _this3.getCategorias();
      })["catch"](function (error) {
        alert(error);
      });
    }
  }),
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])(["listaCategorias"])), {}, {
    sortedCats: function sortedCats() {
      var _this4 = this;

      return this.listaCategorias.sort(function (a, b) {
        var modifier = 1;
        if (_this4.currentSortDir === "desc") modifier = -1;
        if (a[_this4.currentSort] < b[_this4.currentSort]) return -1 * modifier;
        if (a[_this4.currentSort] > b[_this4.currentSort]) return 1 * modifier;
        return 0;
      }).filter(function (row, index) {
        var start = (_this4.currentPage - 1) * _this4.pageSize;
        var end = _this4.currentPage * _this4.pageSize;
        if (index >= start && index < end) return true;
      });
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Categorias.vue?vue&type=template&id=4d1d33ff&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Categorias.vue?vue&type=template&id=4d1d33ff& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("b-container", [
    _c("div", { staticClass: "row mt-5" }, [
      _c(
        "div",
        { staticClass: "col-md order-md-1" },
        [
          _c("h4", { staticClass: "mb-3" }, [
            _vm._v("Modulo Categorias " + _vm._s(_vm.categoria.nombre))
          ]),
          _vm._v(" "),
          _c(
            "div",
            [
              _c(
                "b-form",
                { attrs: { inline: "" } },
                [
                  _c(
                    "label",
                    {
                      staticClass: "sr-only",
                      attrs: { for: "nombre-categoria" }
                    },
                    [_vm._v("Nombre de categoria")]
                  ),
                  _vm._v(" "),
                  _c("b-input", {
                    staticClass: "mb-2 mr-sm-2 mb-sm-0",
                    attrs: {
                      id: "nombre-categoria",
                      placeholder: "Nombre de categoria"
                    },
                    model: {
                      value: _vm.categoria.nombre,
                      callback: function($$v) {
                        _vm.$set(_vm.categoria, "nombre", $$v)
                      },
                      expression: "categoria.nombre"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "sr-only", attrs: { for: "num-cocina" } },
                    [_vm._v("Username")]
                  ),
                  _vm._v(" "),
                  _c("b-input", {
                    staticClass: "mb-2 mr-sm-2 mb-sm-0",
                    attrs: {
                      id: "num-cocina",
                      placeholder: "Número de cocina"
                    },
                    model: {
                      value: _vm.categoria.cocina,
                      callback: function($$v) {
                        _vm.$set(_vm.categoria, "cocina", $$v)
                      },
                      expression: "categoria.cocina"
                    }
                  }),
                  _vm._v(" "),
                  !_vm.editar
                    ? _c(
                        "b-button",
                        {
                          attrs: { variant: "success" },
                          on: {
                            click: function($event) {
                              return _vm.addCategory()
                            }
                          }
                        },
                        [_vm._v("Agregar")]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.editar
                    ? _c(
                        "b-button",
                        {
                          attrs: { variant: "warning" },
                          on: {
                            click: function($event) {
                              return _vm.editCategory()
                            }
                          }
                        },
                        [_vm._v("Editar")]
                      )
                    : _vm._e()
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("table", { staticClass: "table table-striped mt-5" }, [
            _c("thead", [
              _c("tr", [
                _c("th", { attrs: { scope: "col" } }, [_vm._v("#")]),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("nombre")
                      }
                    }
                  },
                  [_vm._v("Nombre")]
                ),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("cocina")
                      }
                    }
                  },
                  [_vm._v("Cocina")]
                ),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Acciones")])
              ])
            ]),
            _vm._v(" "),
            _c(
              "tbody",
              _vm._l(_vm.sortedCats, function(data, index) {
                return _c("tr", { key: index }, [
                  _c("td", [_vm._v(_vm._s(index))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(data.cat_nombre))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(data.cat_cocina))]),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _c(
                        "b-button",
                        {
                          staticClass: "mx-1",
                          attrs: {
                            variant: "warning",
                            "data-toggle": "modal",
                            "data-target": "#editarModal"
                          },
                          on: {
                            click: function($event) {
                              return _vm.setVal(
                                data.cat_id,
                                data.cat_nombre,
                                data.cat_cocina
                              )
                            }
                          }
                        },
                        [_vm._v("Editar")]
                      ),
                      _vm._v(" "),
                      _c(
                        "b-button",
                        {
                          staticClass: "mx-1",
                          attrs: { variant: "danger" },
                          on: {
                            click: function($event) {
                              return _vm.deleteCategoria(data.cat_id)
                            }
                          }
                        },
                        [_vm._v("Borrar")]
                      )
                    ],
                    1
                  )
                ])
              }),
              0
            )
          ]),
          _vm._v(" "),
          _c("b-pagination", {
            staticClass: "mt-4",
            attrs: {
              "total-rows": _vm.listaCategorias.length,
              "per-page": _vm.pageSize,
              "first-number": "",
              "last-number": ""
            },
            model: {
              value: _vm.currentPage,
              callback: function($$v) {
                _vm.currentPage = $$v
              },
              expression: "currentPage"
            }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Categorias.vue":
/*!*******************************************!*\
  !*** ./resources/js/views/Categorias.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Categorias_vue_vue_type_template_id_4d1d33ff___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Categorias.vue?vue&type=template&id=4d1d33ff& */ "./resources/js/views/Categorias.vue?vue&type=template&id=4d1d33ff&");
/* harmony import */ var _Categorias_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Categorias.vue?vue&type=script&lang=js& */ "./resources/js/views/Categorias.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Categorias_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Categorias_vue_vue_type_template_id_4d1d33ff___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Categorias_vue_vue_type_template_id_4d1d33ff___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Categorias.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Categorias.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/views/Categorias.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Categorias_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Categorias.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Categorias.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Categorias_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Categorias.vue?vue&type=template&id=4d1d33ff&":
/*!**************************************************************************!*\
  !*** ./resources/js/views/Categorias.vue?vue&type=template&id=4d1d33ff& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Categorias_vue_vue_type_template_id_4d1d33ff___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Categorias.vue?vue&type=template&id=4d1d33ff& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Categorias.vue?vue&type=template&id=4d1d33ff&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Categorias_vue_vue_type_template_id_4d1d33ff___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Categorias_vue_vue_type_template_id_4d1d33ff___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);