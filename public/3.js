(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Clientes.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Clientes.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Clientes",
  data: function data() {
    return {
      listCliente: [],
      cliente: {
        id: 0,
        nombre: "",
        cedula: "",
        telefono: "",
        direccion: ""
      },
      currentSort: "cliente_id",
      currentSortDir: "asc",
      pageSize: 6,
      currentPage: 1
    };
  },
  mounted: function mounted() {
    this.listClienteService();
  },
  methods: {
    nextPage: function nextPage() {
      if (this.currentPage * this.pageSize < this.listCliente.length) this.currentPage++;
    },
    prevPage: function prevPage() {
      if (this.currentPage > 1) this.currentPage--;
    },
    sort: function sort(s) {
      if (s === this.currentSort) {
        this.currentSortDir = this.currentSortDir === "asc" ? "desc" : "asc";
      }

      this.currentSort = s;
    },
    listClienteService: function listClienteService() {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("cliente/list").then(function (response) {
        _this.listCliente = response.data.clientes;
      })["catch"](function (error) {
        alert(error);
      });
      setTimeout(this.listClienteService, 10000);
    },
    editCliente: function editCliente() {
      var _this2 = this;

      var formData = {
        id: this.campoId,
        nombre: this.cliente.nombre,
        cedula: this.cliente.cedula,
        telefono: this.cliente.telefono,
        direccion: this.cliente.direccion
      };
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("cliente/update", formData).then(function (response) {
        alert(response.data.message);
        _this2.campoId = 0;
        _this2.cliente.nombre = "";
        _this2.cliente.cedula = "";
        _this2.cliente.telefono = "";
        _this2.cliente.direccion = "";

        _this2.listClienteService();
      })["catch"](function (error) {
        alert(error);
      });
    },
    setVal: function setVal(val_id, val_nombre, val_cedula, val_telefono, val_direccion) {
      this.campoId = val_id;
      this.cliente.nombre = val_nombre;
      this.cliente.telefono = val_telefono;
      this.cliente.cedula = val_cedula;
      this.cliente.direccion = val_direccion;
    },
    deleteCliente: function deleteCliente(val_id) {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("cliente/delete/" + val_id).then(function (response) {
        _this3.listClienteService();
      })["catch"](function (error) {
        alert(error);
      });
    }
  },
  computed: {
    filteredEntries: function filteredEntries() {
      var _this4 = this;

      return this.listCliente.sort(function (a, b) {
        var modifier = 1;
        if (_this4.currentSortDir === "desc") modifier = -1;
        if (a[_this4.currentSort] < b[_this4.currentSort]) return -1 * modifier;
        if (a[_this4.currentSort] > b[_this4.currentSort]) return 1 * modifier;
        return 0;
      }).filter(function (row, index) {
        var start = (_this4.currentPage - 1) * _this4.pageSize;
        var end = _this4.currentPage * _this4.pageSize;
        if (index >= start && index < end) return true;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nh4.mb-3 input {\r\n  font-size: 16px !important;\r\n  margin-right: 30px;\n}\nh4.mb-3 span:first-child {\r\n  margin-right: 30px;\n}\nh4.mb-3 span:not(:first-child) {\r\n  margin-right: 10px;\r\n  font-size: 16px !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Clientes.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Clientes.vue?vue&type=template&id=144fb958&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Clientes.vue?vue&type=template&id=144fb958& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("b-container", [
    _c("br"),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md order-md-1" },
        [
          _c("h4", { staticClass: "mb-3" }, [_vm._v("Clientes")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mb-4" },
            [
              _c(
                "b-form",
                { attrs: { inline: "" } },
                [
                  _c(
                    "label",
                    { staticClass: "sr-only", attrs: { for: "nombre" } },
                    [_vm._v("Nombre")]
                  ),
                  _vm._v(" "),
                  _c("b-input", {
                    staticClass: "mb-2 mr-sm-2 mb-sm-0",
                    attrs: { id: "nombre", placeholder: "Nombre" },
                    model: {
                      value: _vm.cliente.nombre,
                      callback: function($$v) {
                        _vm.$set(_vm.cliente, "nombre", $$v)
                      },
                      expression: "cliente.nombre"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "sr-only", attrs: { for: "cedula" } },
                    [_vm._v("Cédula")]
                  ),
                  _vm._v(" "),
                  _c("b-input", {
                    staticClass: "mb-2 mr-sm-2 mb-sm-0",
                    attrs: { id: "cedula", placeholder: "Cédula" },
                    model: {
                      value: _vm.cliente.cedula,
                      callback: function($$v) {
                        _vm.$set(_vm.cliente, "cedula", $$v)
                      },
                      expression: "cliente.cedula"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "sr-only", attrs: { for: "telefono" } },
                    [_vm._v("Teléfono")]
                  ),
                  _vm._v(" "),
                  _c("b-input", {
                    staticClass: "mb-2 mr-sm-2 mb-sm-0",
                    attrs: { id: "telefono", placeholder: "Teléfono" },
                    model: {
                      value: _vm.cliente.telefono,
                      callback: function($$v) {
                        _vm.$set(_vm.cliente, "telefono", $$v)
                      },
                      expression: "cliente.telefono"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "sr-only", attrs: { for: "direccion" } },
                    [_vm._v("Dirección")]
                  ),
                  _vm._v(" "),
                  _c("b-input", {
                    staticClass: "mb-2 mr-sm-2 mb-sm-0",
                    attrs: { id: "direccion", placeholder: "Dirección" },
                    model: {
                      value: _vm.cliente.direccion,
                      callback: function($$v) {
                        _vm.$set(_vm.cliente, "direccion", $$v)
                      },
                      expression: "cliente.direccion"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "b-button",
                    {
                      staticClass: "ml-2",
                      attrs: { variant: "warning" },
                      on: {
                        click: function($event) {
                          return _vm.editCliente()
                        }
                      }
                    },
                    [_vm._v("Editar")]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("table", { staticClass: "table t" }, [
            _c("thead", [
              _c("tr", [
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("cliente_id")
                      }
                    }
                  },
                  [_vm._v("Nombre")]
                ),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("cliente_cedula")
                      }
                    }
                  },
                  [_vm._v("Cédula")]
                ),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("cliente_telefono")
                      }
                    }
                  },
                  [_vm._v("Teléfono")]
                ),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Dirección")]),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } })
              ])
            ]),
            _vm._v(" "),
            _c(
              "tbody",
              [
                _vm._l(_vm.filteredEntries, function(data) {
                  return [
                    _c("tr", { key: data }, [
                      _c("td", [_vm._v(_vm._s(data.cliente_nombre))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(data.cliente_cedula))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(data.cliente_telefono))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(data.cliente_direccion))]),
                      _vm._v(" "),
                      _c(
                        "td",
                        [
                          _c(
                            "b-button",
                            {
                              attrs: { variant: "warning" },
                              on: {
                                click: function($event) {
                                  return _vm.setVal(
                                    data.cliente_id,
                                    data.cliente_nombre,
                                    data.cliente_cedula,
                                    data.cliente_telefono,
                                    data.cliente_direccion
                                  )
                                }
                              }
                            },
                            [_vm._v("Editar")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "td",
                        [
                          _c(
                            "b-button",
                            {
                              attrs: { variant: "danger" },
                              on: {
                                click: function($event) {
                                  return _vm.deleteCliente(data.cliente_id)
                                }
                              }
                            },
                            [_vm._v("Borrar")]
                          )
                        ],
                        1
                      )
                    ])
                  ]
                })
              ],
              2
            )
          ]),
          _vm._v(" "),
          _c("b-pagination", {
            staticClass: "mt-4",
            attrs: {
              "total-rows": _vm.listCliente.length,
              "per-page": _vm.pageSize,
              "first-number": "",
              "last-number": ""
            },
            model: {
              value: _vm.currentPage,
              callback: function($$v) {
                _vm.currentPage = $$v
              },
              expression: "currentPage"
            }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Clientes.vue":
/*!*****************************************!*\
  !*** ./resources/js/views/Clientes.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Clientes_vue_vue_type_template_id_144fb958___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Clientes.vue?vue&type=template&id=144fb958& */ "./resources/js/views/Clientes.vue?vue&type=template&id=144fb958&");
/* harmony import */ var _Clientes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Clientes.vue?vue&type=script&lang=js& */ "./resources/js/views/Clientes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Clientes_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Clientes.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Clientes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Clientes_vue_vue_type_template_id_144fb958___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Clientes_vue_vue_type_template_id_144fb958___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Clientes.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Clientes.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/views/Clientes.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Clientes.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Clientes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************!*\
  !*** ./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Clientes.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Clientes.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/Clientes.vue?vue&type=template&id=144fb958&":
/*!************************************************************************!*\
  !*** ./resources/js/views/Clientes.vue?vue&type=template&id=144fb958& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_template_id_144fb958___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Clientes.vue?vue&type=template&id=144fb958& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Clientes.vue?vue&type=template&id=144fb958&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_template_id_144fb958___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_template_id_144fb958___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);