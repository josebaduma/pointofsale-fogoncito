(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Orden.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Orden.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../router */ "./resources/js/router/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Orden",
  data: function data() {
    return {
      listMesas: [],
      listCarrito: [],
      listEmpleados: [],
      listLoadCarrito: [],
      mesaCambioSelected: 0,
      empleadoSelected: 0,
      selectCategoria: 0,
      SelectMesa: this.$route.params.id,
      estadoMesa: 0,
      numOrden: 0,
      descripcion: ""
    };
  },
  mounted: function mounted() {
    this.getCategorias();
    this.getProductos();
    this.listMesasService();
    this.listEmpleadosService();
    this.loadOrderMesa();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])(["listaCategorias", "listaProductos"])),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])(["getCategorias", "getProductos"])), {}, {
    facturar: function facturar() {
      window.location.href = "/orden/pagar/" + this.SelectMesa;
    },
    delete_orden: function delete_orden() {
      if (this.numOrden != 0) {
        axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("orden/delete/" + this.numOrden).then(function (response) {})["catch"](function (error) {
          alert(error);
        });
      }

      this.numOrden = 0;
      this.listCarrito = [];
      this.estadoMesa = 0;
      this.empleadoSelected = 0;
    },
    selectEmpleado: function selectEmpleado() {},
    convertMoney: function convertMoney(value) {
      var formatterPeso = new Intl.NumberFormat("es-CR", {
        style: "currency",
        currency: "CRC",
        minimumFractionDigits: 0
      });
      var valueFinal = formatterPeso.format(value);
      return valueFinal;
    },
    listMesasService: function listMesasService() {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("mesas/list").then(function (response) {
        // cargar datos
        _this.listMesas = response.data.mesas;
      })["catch"](function (error) {
        alert(error);
      });
    },
    listEmpleadosService: function listEmpleadosService() {
      var _this2 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("meseros/list").then(function (response) {
        _this2.listEmpleados = response.data;
      })["catch"](function (error) {
        alert(error);
      });
    },
    addCart: function addCart(item) {
      var itemcar = {
        serial: 0,
        id: item.prod_id,
        nombre: item.prod_name,
        categoria: item.cat_nombre,
        cant: 1,
        precio: item.prod_price,
        descripcion: ""
      };
      this.listCarrito.push(itemcar);
    },
    deleteItem: function deleteItem(i) {
      var item = this.listCarrito.splice(i, 1);

      if (item[0].serial != 0) {
        var formData = {
          serial: item[0].serial
        };
        axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("orden/delete-detalle", formData).then(function (response) {})["catch"](function (error) {
          alert(error);
        });
      }
    },
    cambiarCantidad: function cambiarCantidad(i, type) {
      var dataCar = this.listCarrito;
      var cantd = dataCar[i].cant;

      if (type) {
        cantd = cantd + 1;
      } else if (type == false && cantd >= 1) {
        cantd = cantd - 1;
      }

      if (type == false && cantd >= 1 || type) {
        dataCar[i].cant = cantd;
        this.listCarrito;
      }
    },
    addDescription: function addDescription(i) {
      var dataCar = this.listCarrito;
      dataCar[i].descripcion = document.getElementById("description_" + i).value;
      this.listCarrito;
    },
    onViewTotal: function onViewTotal() {
      var total = 0;
      this.listCarrito.map(function (data) {
        total = total + data.cant * data.precio;
      });
      return this.convertMoney(total);
    },
    onSendOrder: function onSendOrder() {
      var _this3 = this;

      if (this.listCarrito.length >= 1) {
        var total = 0;
        this.listCarrito.map(function (data) {
          total = total + data.cant * data.precio;
        });
        var formData = {
          pedidos: JSON.stringify(this.listCarrito),
          tipo: 0,
          mesa: this.SelectMesa,
          empleado: this.empleadoSelected,
          total: total
        };

        if (this.numOrden == 0) {
          axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("orden/create", formData).then(function (response) {
            _this3.$swal.fire("Enviado exitosamente!", "Se envio los ordenó exitosamente", "success");

            _this3.numOrden = response.data.orden_id;

            _this3.loadOrderMesa();
          })["catch"](function (error) {
            alert(error);
          });
        } else {
          formData.orden_id = this.numOrden;
          axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("orden/update", formData).then(function (response) {
            _this3.$swal.fire("Enviado exitosamente!", "Se envio los ordenó exitosamente", "success");

            _this3.loadOrderMesa();
          })["catch"](function (error) {
            alert(error);
          });
        }
      }
    },
    selectCambioMesa: function selectCambioMesa() {
      var _this4 = this;

      var data = {
        id: this.numOrden,
        actual: this.SelectMesa,
        destino: this.mesaCambioSelected
      };
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("orden/change-mesa/", data).then(function (response) {
        _this4.listCarrito = [];

        _this4.$swal.fire("Cambio exitoso!", "Se cambio de mesa su orden exitosamente!", "success");

        _router__WEBPACK_IMPORTED_MODULE_2__["default"].push({
          name: "Orden",
          params: {
            id: _this4.mesaCambioSelected
          }
        });
      })["catch"](function (error) {
        console.log(error);
      });
    },
    loadOrderMesa: function loadOrderMesa() {
      var _this5 = this;

      this.listCarrito = [];
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("orden/load/" + this.SelectMesa).then(function (response) {
        var ordenResp = response.data.pop();
        var pedidos = ordenResp.pedidos;
        _this5.estadoMesa = ordenResp.mesas_estado;
        _this5.empleadoSelected = ordenResp.ord_empleado;
        _this5.numOrden = ordenResp.ord_id;
        pedidos.forEach(function (itemCar) {
          var item = {
            serial: itemCar["detalle_orden_serial"],
            id: itemCar["prod_id"],
            nombre: itemCar["prod_name"],
            categoria: itemCar["cat_nombre"],
            cant: itemCar["detalle_orden_cantidad"],
            precio: itemCar["detalle_orden_valor"],
            descripcion: itemCar["detalle_orden_descripcion"]
          };

          _this5.listCarrito.push(item);
        });
      })["catch"](function (error) {
        console.log(error);
      });
    },
    print_orden: function print_orden() {
      var _this6 = this;

      if (this.listCarrito.length >= 1) {
        var total = 0;
        this.listCarrito.map(function (data) {
          total = total + data.cant * data.precio;
        });
        var formData = {
          pedidos: JSON.stringify(this.listCarrito),
          tipo: 0,
          mesa: this.SelectMesa,
          empleado: this.empleadoSelected,
          empleado_nombre: this.listEmpleados[this.empleadoSelected],
          total: total
        };
        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("orden/print/" + this.numOrden + "/" + this.SelectMesa).then(function (response) {
          _this6.$swal.fire("Enviado exitosamente!", "Se envio los ordenó exitosamente", "success");
        })["catch"](function (error) {
          console.log(error);
        });
      }
    }
  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.btn {\r\n  margin-right: 5px;\r\n  margin-bottom: 5px;\n}\n.pointer {\r\n  cursor: pointer;\n}\n@media only screen and (min-width: 1024px) {\n#productModal .modal-dialog {\r\n    max-width: 800px;\n}\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Orden.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Orden.vue?vue&type=template&id=61dff8bb&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Orden.vue?vue&type=template&id=61dff8bb& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("br"),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "row" },
      [
        _c("div", { staticClass: "col-md-12 order-md-2 mb-4" }, [
          _c(
            "h4",
            {
              staticClass:
                "d-flex justify-content-between align-items-center mb-3"
            },
            [
              _c("span", { staticClass: "text-muted" }, [
                _vm._v("Orden Mesa " + _vm._s(_vm.SelectMesa))
              ]),
              _vm._v(" "),
              _c(
                "b-button",
                {
                  directives: [
                    {
                      name: "b-modal",
                      rawName: "v-b-modal.productModal",
                      modifiers: { productModal: true }
                    }
                  ],
                  staticClass: "btn btn-secondary"
                },
                [_vm._v("Agregar producto")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-secondary",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.delete_orden()
                    }
                  }
                },
                [_vm._v("Borrar")]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "btn btn-secondary text-white",
                  on: { click: _vm.facturar }
                },
                [_vm._v("Facturar")]
              ),
              _vm._v(" "),
              _c("span", { staticClass: "badge badge-secondary badge-pill" }, [
                _vm._v(_vm._s(_vm.listCarrito.length))
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "ul",
            { staticClass: "list-group mb-3" },
            [
              _c(
                "li",
                {
                  staticClass:
                    "list-group-item d-flex justify-content-between lh-condensed"
                },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("span", { staticClass: "text-muted" }, [
                    _vm._v(_vm._s(_vm.listCarrito.lentgh))
                  ])
                ]
              ),
              _vm._v(" "),
              _vm._l(_vm.listCarrito, function(itemcart, i) {
                return _c(
                  "li",
                  {
                    key: i,
                    staticClass:
                      "list-group-item d-flex justify-content-between lh-condensed"
                  },
                  [
                    _c("div", { staticClass: "col-md-3" }, [
                      _c("h6", { staticClass: "my-0" }, [
                        _vm._v(_vm._s(itemcart.nombre))
                      ]),
                      _vm._v(" "),
                      _c("small", { staticClass: "text-muted" }, [
                        _vm._v(_vm._s(itemcart.categoria))
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-2" }, [
                      _c(
                        "div",
                        [
                          _c("b-icon-dash-circle", {
                            staticClass: "pointer",
                            on: {
                              click: function($event) {
                                return _vm.cambiarCantidad(i, false)
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("span", { staticClass: "text-muted mx-2" }, [
                            _vm._v(_vm._s(itemcart.cant))
                          ]),
                          _vm._v(" "),
                          _c("b-icon-plus-circle", {
                            staticClass: "material-icons pointer",
                            on: {
                              click: function($event) {
                                return _vm.cambiarCantidad(i, true)
                              }
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("span", { staticClass: "text-muted" }, [
                        _vm._v(
                          _vm._s(
                            _vm.convertMoney(itemcart.cant * itemcart.precio)
                          )
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6" }, [
                      _c("input", {
                        attrs: {
                          type: "text",
                          id: "description_" + i,
                          name: "description_" + i,
                          placeholder: "Descripción",
                          maxlength: "128",
                          size: "48"
                        },
                        domProps: { value: itemcart.descripcion },
                        on: {
                          change: function($event) {
                            return _vm.addDescription(i)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("b-icon-trash", {
                      staticClass: "pointer",
                      on: {
                        click: function($event) {
                          return _vm.deleteItem(i)
                        }
                      }
                    })
                  ],
                  1
                )
              }),
              _vm._v(" "),
              _c(
                "li",
                {
                  staticClass: "list-group-item d-flex justify-content-between"
                },
                [
                  _c("span", [_vm._v("Total")]),
                  _vm._v(" "),
                  _c("strong", [_vm._v(_vm._s(_vm.onViewTotal()))])
                ]
              ),
              _vm._v(" "),
              _c(
                "li",
                {
                  staticClass: "list-group-item d-flex justify-content-between"
                },
                [
                  _c("span", [_vm._v("Seleccion Mesero:")]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.empleadoSelected,
                          expression: "empleadoSelected"
                        }
                      ],
                      staticStyle: { width: "256px" },
                      attrs: { name: "empleadoSelected", width: "256" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.empleadoSelected = $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          },
                          function($event) {
                            return _vm.selectEmpleado()
                          }
                        ]
                      }
                    },
                    _vm._l(_vm.listEmpleados, function(empleado, i) {
                      return _c(
                        "option",
                        { key: i, domProps: { value: empleado.empleado_id } },
                        [_vm._v(_vm._s(empleado.empleado_nombre))]
                      )
                    }),
                    0
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "li",
                {
                  staticClass: "list-group-item d-flex justify-content-between"
                },
                [
                  _c("span", [_vm._v("Cambiar de Mesa:")]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.mesaCambioSelected,
                          expression: "mesaCambioSelected"
                        }
                      ],
                      staticStyle: { width: "256px" },
                      attrs: { name: "mesaCambioSelected", width: "256" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.mesaCambioSelected = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    _vm._l(_vm.listMesas, function(mesa, i) {
                      return _c(
                        "option",
                        { key: i, domProps: { value: mesa.mesas_id } },
                        [_vm._v("Mesa " + _vm._s(mesa.mesas_id))]
                      )
                    }),
                    0
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-info",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return _vm.selectCambioMesa()
                        }
                      }
                    },
                    [_vm._v("Cambiar")]
                  )
                ]
              )
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-info btn-lg btn-block",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  return _vm.onSendOrder()
                }
              }
            },
            [_vm._v("PROCESAR PEDIDO")]
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-info btn-lg btn-block",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  return _vm.print_orden()
                }
              }
            },
            [_vm._v("IMPRIMIR COMANDA")]
          )
        ]),
        _vm._v(" "),
        _c("b-modal", { attrs: { id: "productModal", title: "Menú" } }, [
          _c(
            "div",
            { staticClass: "d-block col-md-12 order-md-1" },
            [
              _c(
                "button",
                {
                  staticClass: "btn btn-secondary",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      _vm.selectCategoria = 0
                    }
                  }
                },
                [_vm._v("Todos")]
              ),
              _vm._v(" "),
              _vm._l(_vm.listaCategorias, function(cat, i) {
                return _c(
                  "button",
                  {
                    key: i,
                    staticClass: "btn btn-secondary",
                    attrs: { type: "button" },
                    on: {
                      click: function($event) {
                        _vm.selectCategoria = cat.cat_id
                      }
                    }
                  },
                  [_vm._v(_vm._s(cat.cat_nombre))]
                )
              }),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "row" },
                _vm._l(_vm.listaProductos, function(prod, i) {
                  return _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value:
                            prod.prod_categoria == _vm.selectCategoria ||
                            _vm.selectCategoria == 0,
                          expression:
                            "prod.prod_categoria==selectCategoria||selectCategoria==0"
                        }
                      ],
                      key: i,
                      staticClass: "card col-md-4",
                      staticStyle: { padding: "0px" }
                    },
                    [
                      _c("div", { staticClass: "card-body" }, [
                        _c("h5", { staticClass: "card-title" }, [
                          _vm._v(_vm._s(prod.prod_name))
                        ]),
                        _vm._v(" "),
                        _c("p", { staticClass: "card-text" }, [
                          _vm._v(_vm._s(prod.prod_description))
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "btn btn-secondary",
                            attrs: { href: "#" },
                            on: {
                              click: function($event) {
                                return _vm.addCart(prod)
                              }
                            }
                          },
                          [_vm._v(_vm._s(_vm.convertMoney(prod.prod_price)))]
                        )
                      ])
                    ]
                  )
                }),
                0
              )
            ],
            2
          )
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h6", { staticClass: "my-0" }, [_vm._v("Producto")]),
      _vm._v(" "),
      _c("small", { staticClass: "text-muted" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Orden.vue":
/*!**************************************!*\
  !*** ./resources/js/views/Orden.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Orden_vue_vue_type_template_id_61dff8bb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Orden.vue?vue&type=template&id=61dff8bb& */ "./resources/js/views/Orden.vue?vue&type=template&id=61dff8bb&");
/* harmony import */ var _Orden_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Orden.vue?vue&type=script&lang=js& */ "./resources/js/views/Orden.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Orden_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Orden.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Orden_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Orden_vue_vue_type_template_id_61dff8bb___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Orden_vue_vue_type_template_id_61dff8bb___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Orden.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Orden.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/views/Orden.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Orden.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Orden.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Orden.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Orden.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/Orden.vue?vue&type=template&id=61dff8bb&":
/*!*********************************************************************!*\
  !*** ./resources/js/views/Orden.vue?vue&type=template&id=61dff8bb& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_template_id_61dff8bb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Orden.vue?vue&type=template&id=61dff8bb& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Orden.vue?vue&type=template&id=61dff8bb&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_template_id_61dff8bb___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Orden_vue_vue_type_template_id_61dff8bb___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);