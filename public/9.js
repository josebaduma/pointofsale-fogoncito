(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Productos.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Productos.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../firebase */ "./resources/js/firebase.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Productos",
  data: function data() {
    return {
      producto: {
        id: 0,
        nombre: "",
        precio: "",
        categoria: 0,
        tipo: 0
      },
      editar: false,
      currentSort: "prod_name",
      currentSortDir: "asc",
      pageSize: 6,
      currentPage: 1
    };
  },
  created: function created() {
    this.getCategorias();
    this.getProductos();
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])(["getCategorias", "getProductos"])), {}, {
    sort: function sort(s) {
      //if s == current sort, reverse
      if (s === this.currentSort) {
        this.currentSortDir = this.currentSortDir === "asc" ? "desc" : "asc";
      }

      this.currentSort = s;
    },
    onSelectCategories: function onSelectCategories(event) {
      this.producto.categoria = event.target.value;
    },
    onSelectTipo: function onSelectTipo(event) {
      this.producto.tipo = event.target.value;
    },
    getTypeMenu: function getTypeMenu(type) {
      if (type == 0) {
        return " Restaurante";
      } else {
        return "Express";
      }
    },
    addProduct: function addProduct() {
      var _this = this;

      if (this.producto.nombre == "") {
        alert("Completa los campo de nombre ");
      } else if (this.producto.precio == "") {
        alert("Completa los campo de precio");
      } else if (this.producto.categoria == 0) {
        alert("Selecciona la categoria");
      } else {
        var formData = {
          name: this.producto.nombre,
          categorie: this.producto.categoria,
          price: this.producto.precio,
          type: this.producto.tipo
        };
        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("product/create", formData).then(function (response) {
          console.log(response.data.message);
        })["catch"](function (error) {
          console.log(error);
        });
        var ref = _firebase__WEBPACK_IMPORTED_MODULE_2__["db"].collection(this.usuario.name).doc("productos");
        ref.get().then(function (res) {
          var productos = res.data().productos;
          productos.push(_this.producto.nombre);
          ref.update({
            productos: productos
          });
          _this.producto.id = 0;
          _this.producto.nombre = "";
          _this.producto.categoria = 0;
          _this.producto.precio = 0;
          _this.producto.tipo = 0;

          _this.getProductos();
        })["catch"](function (error) {
          console.log(error);
        });
      }
    },
    editProduct: function editProduct() {
      var _this2 = this;

      var formData = {
        id: this.producto.id,
        name: this.producto.nombre,
        category: this.producto.categoria,
        price: this.producto.precio,
        type: this.producto.tipo
      };
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("product/update", formData).then(function (response) {
        alert(response.data.message);
        _this2.editar = false;
        _this2.producto.id = 0;
        _this2.producto.nombre = "";
        _this2.producto.categoria = 0;
        _this2.producto.precio = 0;
        _this2.producto.tipo = 0;

        _this2.getProductos();
      })["catch"](function (error) {
        alert(error);
      });
    },
    setVal: function setVal(val_id, val_name, val_price, val_menu, val_cat) {
      this.producto.id = val_id;
      this.producto.nombre = val_name;
      this.producto.precio = val_price;
      this.producto.categoria = val_cat;
      this.producto.tipo = val_menu;
      this.editar = true;
    },
    deleteProducto: function deleteProducto(val_id) {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("product/delete/" + val_id).then(function (response) {
        _this3.getProductos();
      })["catch"](function (error) {
        alert(error);
      });
    }
  }),
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])(["listaProductos", "listaCategorias", "usuario"])), {}, {
    sortedProds: function sortedProds() {
      var _this4 = this;

      return this.listaProductos.sort(function (a, b) {
        var modifier = 1;
        if (_this4.currentSortDir === "desc") modifier = -1;
        if (a[_this4.currentSort] < b[_this4.currentSort]) return -1 * modifier;
        if (a[_this4.currentSort] > b[_this4.currentSort]) return 1 * modifier;
        return 0;
      }).filter(function (row, index) {
        var start = (_this4.currentPage - 1) * _this4.pageSize;
        var end = _this4.currentPage * _this4.pageSize;
        if (index >= start && index < end) return true;
      });
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Productos.vue?vue&type=template&id=f067a8f8&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Productos.vue?vue&type=template&id=f067a8f8& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("b-container", [
    _c("br"),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md order-md-1" },
        [
          _c("h4", { staticClass: "mb-3" }, [_vm._v("Modulo Producto")]),
          _vm._v(" "),
          _c(
            "div",
            [
              _c(
                "b-form",
                { attrs: { inline: "" } },
                [
                  _c(
                    "label",
                    { staticClass: "sr-only", attrs: { for: "prod-nombre" } },
                    [_vm._v("Nombre")]
                  ),
                  _vm._v(" "),
                  _c("b-input", {
                    staticClass: "mb-2 mr-sm-2 mb-sm-0",
                    attrs: { id: "prod-nombre", placeholder: "Nombre" },
                    model: {
                      value: _vm.producto.nombre,
                      callback: function($$v) {
                        _vm.$set(_vm.producto, "nombre", $$v)
                      },
                      expression: "producto.nombre"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "sr-only", attrs: { for: "prod-precio" } },
                    [_vm._v("Precio")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-input-group",
                    {
                      staticClass: "mb-2 mr-sm-2 mb-sm-0",
                      attrs: { prepend: "₡", append: ".00" }
                    },
                    [
                      _c("b-input", {
                        attrs: { id: "prod-precio", placeholder: "Precio" },
                        model: {
                          value: _vm.producto.precio,
                          callback: function($$v) {
                            _vm.$set(_vm.producto, "precio", $$v)
                          },
                          expression: "producto.precio"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "sr-only",
                      attrs: { for: "prod-categoria" }
                    },
                    [_vm._v("Categoria")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-select",
                    {
                      staticClass: "mb-2 mr-sm-2 mb-sm-0",
                      attrs: { id: "prod-categoria", required: "" },
                      model: {
                        value: _vm.producto.categoria,
                        callback: function($$v) {
                          _vm.$set(_vm.producto, "categoria", $$v)
                        },
                        expression: "producto.categoria"
                      }
                    },
                    _vm._l(_vm.listaCategorias, function(cat, index) {
                      return _c(
                        "b-form-select-option",
                        { key: index, attrs: { value: cat.cat_id } },
                        [_vm._v(_vm._s(cat.cat_nombre))]
                      )
                    }),
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "sr-only", attrs: { for: "prod-tipo" } },
                    [_vm._v("Tipo de Menú")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-select",
                    {
                      staticClass: "mb-2 mr-sm-2 mb-sm-0",
                      attrs: { id: "prod-tipo", required: "" },
                      model: {
                        value: _vm.producto.tipo,
                        callback: function($$v) {
                          _vm.$set(_vm.producto, "tipo", $$v)
                        },
                        expression: "producto.tipo"
                      }
                    },
                    [
                      _c("b-form-select-option", { attrs: { value: 0 } }, [
                        _vm._v("Restaurante")
                      ]),
                      _vm._v(" "),
                      _c("b-form-select-option", { attrs: { value: 1 } }, [
                        _vm._v("Express")
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  !_vm.editar
                    ? _c(
                        "b-button",
                        {
                          attrs: { variant: "success" },
                          on: {
                            click: function($event) {
                              return _vm.addProduct()
                            }
                          }
                        },
                        [_vm._v("Agregar")]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.editar
                    ? _c(
                        "b-button",
                        {
                          attrs: { variant: "warning" },
                          on: {
                            click: function($event) {
                              return _vm.editProduct()
                            }
                          }
                        },
                        [_vm._v("Editar")]
                      )
                    : _vm._e()
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c("table", { staticClass: "table table-striped" }, [
            _c("thead", [
              _c("tr", [
                _c("th", { attrs: { scope: "col" } }, [_vm._v("#")]),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("prod_name")
                      }
                    }
                  },
                  [_vm._v("Producto")]
                ),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("prod_price")
                      }
                    }
                  },
                  [_vm._v("Precio")]
                ),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("cat_nombre")
                      }
                    }
                  },
                  [_vm._v("Categoria")]
                ),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Menú")]),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Acciones")]),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } })
              ])
            ]),
            _vm._v(" "),
            _c(
              "tbody",
              _vm._l(_vm.sortedProds, function(data, index) {
                return _c("tr", { key: index }, [
                  _c("td", [_vm._v(_vm._s(data.prod_id))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(data.prod_name))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(data.prod_price))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(data.cat_nombre))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.getTypeMenu(data.prod_menu)))]),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _c(
                        "b-button",
                        {
                          attrs: {
                            variant: "warning",
                            "data-toggle": "modal",
                            "data-target": "#editarModal"
                          },
                          on: {
                            click: function($event) {
                              return _vm.setVal(
                                data.prod_id,
                                data.prod_name,
                                data.prod_price,
                                data.prod_menu,
                                data.cat_id
                              )
                            }
                          }
                        },
                        [_vm._v("Editar")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _c(
                        "b-button",
                        {
                          attrs: { variant: "danger" },
                          on: {
                            click: function($event) {
                              return _vm.deleteProducto(data.prod_id)
                            }
                          }
                        },
                        [_vm._v("Borrar")]
                      )
                    ],
                    1
                  )
                ])
              }),
              0
            )
          ]),
          _vm._v(" "),
          _c("b-pagination", {
            staticClass: "mt-4",
            attrs: {
              "total-rows": _vm.listaProductos.length,
              "per-page": _vm.pageSize,
              "first-number": "",
              "last-number": ""
            },
            model: {
              value: _vm.currentPage,
              callback: function($$v) {
                _vm.currentPage = $$v
              },
              expression: "currentPage"
            }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Productos.vue":
/*!******************************************!*\
  !*** ./resources/js/views/Productos.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Productos_vue_vue_type_template_id_f067a8f8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Productos.vue?vue&type=template&id=f067a8f8& */ "./resources/js/views/Productos.vue?vue&type=template&id=f067a8f8&");
/* harmony import */ var _Productos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Productos.vue?vue&type=script&lang=js& */ "./resources/js/views/Productos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Productos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Productos_vue_vue_type_template_id_f067a8f8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Productos_vue_vue_type_template_id_f067a8f8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Productos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Productos.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/views/Productos.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Productos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Productos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Productos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Productos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Productos.vue?vue&type=template&id=f067a8f8&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/Productos.vue?vue&type=template&id=f067a8f8& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Productos_vue_vue_type_template_id_f067a8f8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Productos.vue?vue&type=template&id=f067a8f8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Productos.vue?vue&type=template&id=f067a8f8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Productos_vue_vue_type_template_id_f067a8f8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Productos_vue_vue_type_template_id_f067a8f8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);