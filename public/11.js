(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

    /***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Facturar.vue?vue&type=script&lang=js&":
    /*!**************************************************************************************************************************************************************!*\
      !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Facturar.vue?vue&type=script&lang=js& ***!
      \**************************************************************************************************************************************************************/
    /*! exports provided: default */
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    /* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
    /* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
    /* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
    /* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
    /* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../router */ "./resources/js/router/index.js");
    /* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../firebase */ "./resources/js/firebase.js");
    /* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
    /* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
    
    
    function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
    
    function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
    
    function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }
    
    function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
    
    function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
    
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    
    
    
    
    
    /* harmony default export */ __webpack_exports__["default"] = ({
      name: "Facturar",
      data: function data() {
        return {
          listOrden: [],
          listCarrito: [],
          listCliente: [],
          estadoMesa: 1,
          empleado: "",
          empleadoSelected: 0,
          selectCategoria: 0,
          SelectMesa: this.$route.params.id,
          numOrden: 0,
          num_cuenta: 0,
          cliente_nombre: "Cliente",
          cliente_cedula: 0,
          cliente_telefono: 0,
          cliente_direccion: "Dirección",
          selected: "",
          checkImpuestos: false
        };
      },
      methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_5__["mapActions"])(["getInventario"])), {}, {
        addAllOrder: function addAllOrder() {
          if (this.listOrden.length > 0) {
            this.listCarrito = this.listOrden;
            this.listOrden = [];
          }
        },
        removeAllOrder: function removeAllOrder() {
          if (this.listCarrito.length > 0) {
            this.listOrden = this.listCarrito;
            this.listCarrito = [];
          }
        },
        convertMoney: function convertMoney(value) {
          var formatterPeso = new Intl.NumberFormat("es-CR", {
            style: "currency",
            currency: "CRC",
            minimumFractionDigits: 0
          });
          var valueFinal = formatterPeso.format(value);
          return valueFinal;
        },
        changePrice: function changePrice(i) {
          var dataCar = this.listCarrito;
          dataCar[i].precio = document.getElementById("precio_" + i).value;
          this.listCarrito;
        },
        listEmpleado: function listEmpleado() {
          var _this = this;
    
          var empleadoData = [];
          axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("meseros/empleado-mesa/" + this.SelectMesa).then(function (response) {
            _this.empleado = response.data.pop().empleado_nombre;
          })["catch"](function (error) {
            console.log(error);
          });
        },
        listClienteService: function listClienteService() {
          var _this2 = this;
    
          axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("cliente/list").then(function (response) {
            _this2.listCliente = response.data.clientes;
          })["catch"](function (error) {
            alert(error);
          });
        },
        loadOrden: function loadOrden() {
          var _this3 = this;
    
          axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("orden/load/" + this.SelectMesa).then(function (response) {
            var ordenResp = response.data.pop();
            var pedidos = ordenResp.pedidos;
            _this3.estadoMesa = ordenResp.mesas_estado;
            _this3.empleadoSelected = ordenResp.ord_empleado;
            _this3.numOrden = ordenResp.ord_id;
            pedidos.forEach(function (itemCar) {
              var item = {
                id: itemCar["prod_id"],
                nombre: itemCar["prod_name"],
                categoria: itemCar["cat_nombre"],
                descripcion: itemCar["detalle_orden_descripcion"],
                cant: itemCar["detalle_orden_cantidad"],
                precio: itemCar["detalle_orden_valor"]
              };
    
              _this3.listOrden.push(item);
            });
          })["catch"](function (error) {
            console.log(error);
          });
        },
        addCart: function addCart(i) {
          var product = this.listOrden[i];
          var productCarrito = this.listCarrito.find(function (item) {
            return item.id === product.id;
          });
    
          if (productCarrito != null && product.cant > 1) {
            product.cant--;
            productCarrito.cant++;
          } else if (productCarrito != null && product.cant == 1) {
            productCarrito.cant++;
            var removed = this.listOrden.splice(i, 1);
          } else if (productCarrito == null && product.cant > 1) {
            var item = {
              id: product.id,
              nombre: product.nombre,
              categoria: product.categoria,
              descripcion: product.descripcion,
              cant: 1,
              precio: product.precio
            };
            this.listCarrito.push(item);
            product.cant--;
          } else {
            var removed = this.listOrden.splice(i, 1);
            this.listCarrito.push(removed[0]);
          }
        },
        deleteItem: function deleteItem(i) {
          var product = this.listCarrito[i];
          var productOrden = this.listOrden.find(function (item) {
            return item.id === product.id;
          });
    
          if (productOrden != null && product.cant > 1) {
            product.cant--;
            productOrden.cant++;
          } else if (productOrden != null && product.cant == 1) {
            productOrden.cant++;
            var removed = this.listCarrito.splice(i, 1);
          } else if (productOrden == null && product.cant > 1) {
            var item = {
              id: product.id,
              nombre: product.nombre,
              categoria: product.categoria,
              descripcion: product.descripcion,
              cant: 1,
              precio: product.precio
            };
            this.listOrden.push(item);
            product.cant--;
          } else {
            var removed = this.listCarrito.splice(i, 1);
            this.listOrden.push(removed[0]);
          }
        },
        onViewSubTotal: function onViewSubTotal() {
          var total = 0;
          this.listCarrito.map(function (data) {
            total = total + data.cant * data.precio;
          });
          return this.convertMoney(total);
        },
        onViewImp: function onViewImp() {
          var total = 0;
          var percentage = 0;
          this.listCarrito.map(function (data) {
            total = total + data.cant * data.precio;
          });
    
          if (this.checkImpuestos) {
            percentage = 0.1;
          }
    
          return this.convertMoney(total * percentage);
        },
        onViewTotal: function onViewTotal() {
          var total = 0;
          var percentage = 1;
          this.listCarrito.map(function (data) {
            total = total + data.cant * data.precio;
          });
    
          if (this.checkImpuestos) {
            percentage = 1.1;
          }
    
          return this.convertMoney(total * percentage);
        },
        onSendOrder: function onSendOrder() {
          var _this4 = this;
    
          var percentage = 0;
    
          if (this.listCarrito.length >= 1) {
            var total = 0;
            this.listCarrito.map(function (data) {
              total = total + data.cant * data.precio;
            });
    
            if (this.checkImpuestos) {
              percentage = 0.1;
            }
    
            var formData = {
              pedidos: JSON.stringify(this.listCarrito),
              mesa: this.SelectMesa,
              tipo: 0,
              empleado: this.empleadoSelected,
              cedula: this.cliente_cedula,
              cliente: this.cliente_nombre,
              telefono: this.cliente_telefono,
              subtotal: total,
              impuesto: total * percentage,
              total: total * (1 + percentage)
            };
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("ventas/create", formData).then(function (response) {
              _this4.num_cuenta = response.data.cuenta_id;
    
              _this4.toFirestore();
    
              _this4.$swal.fire("Enviado exitosamente!", "Se envio los ordenó exitosamente", "success");
            })["catch"](function (error) {
              console.log(error);
            });
    
            if (this.listOrden.length == 0) {
              var mesa = {
                idmesa: this.SelectMesa,
                estado: 0
              };
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.put("mesas/update", mesa).then(function (response) {
                axios__WEBPACK_IMPORTED_MODULE_1___default.a["delete"]("orden/delete/" + _this4.numOrden).then(function (response) {})["catch"](function (error) {
                  alert(error);
                });
              })["catch"](function (error) {
                alert(error);
              });
              _router__WEBPACK_IMPORTED_MODULE_2__["default"].push({
                name: "Home"
              });
            }
          }
        },
        toFirestore: function toFirestore() {
          var _this5 = this;
    
          return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
            var percentage, total, venta, ref, res;
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    percentage = 0;
                    total = 0;
    
                    _this5.listCarrito.map(function (data) {
                      total = total + data.cant * data.precio;
                    });
    
                    if (_this5.checkImpuestos) {
                      percentage = 0.1;
                    }
    
                    venta = {
                      pedidos: _this5.listCarrito,
                      mesa: _this5.SelectMesa,
                      tipo: "Restaurante",
                      fecha: moment__WEBPACK_IMPORTED_MODULE_4___default()().format("DD/MM/YYYY HH:mm:ss"),
                      empleado: _this5.empleado,
                      cliente: {
                        cedula: _this5.cliente_cedula,
                        nombre: _this5.cliente_nombre,
                        telefono: _this5.cliente_telefono,
                        direccion: ""
                      },
                      subtotal: total,
                      impuesto: total * percentage,
                      total: parseInt(total * (1 + percentage))
                    };
                    _context2.prev = 5;
                    console.log("Carrito: ".concat(_this5.listCarrito));
    
                    _.forEach(_this5.listCarrito, /*#__PURE__*/function () {
                      var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(detalle) {
                        var nombre, item, cant, ref, res;
                        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                          while (1) {
                            switch (_context.prev = _context.next) {
                              case 0:
                                nombre = detalle.nombre;
                                console.log("Nombre: ".concat(nombre));
                                item = _.find(_this5.inventario, function (data) {
                                  return _.includes(data.productos, nombre);
                                });
                                cant = item.cantidad - detalle.cant * item.relacion;
                                ref = _firebase__WEBPACK_IMPORTED_MODULE_3__["db"].collection(_this5.usuario.name).doc("inventario").collection("inventario").doc(item.id);
                                _context.next = 7;
                                return ref.update({
                                  cantidad: cant
                                });
    
                              case 7:
                                res = _context.sent;
    
                              case 8:
                              case "end":
                                return _context.stop();
                            }
                          }
                        }, _callee);
                      }));
    
                      return function (_x) {
                        return _ref.apply(this, arguments);
                      };
                    }());
    
                    ref = _firebase__WEBPACK_IMPORTED_MODULE_3__["db"].collection(_this5.usuario.name).doc("ventas");
                    _context2.next = 11;
                    return ref.collection("ventas").add(venta);
    
                  case 11:
                    res = _context2.sent;
                    console.log("Added document with ID: ", res.id);
                    _this5.empleado = "";
                    _this5.cliente_cedula = 0;
                    _this5.cliente_nombre = "Cliente";
                    _this5.cliente_telefono = 0;
                    _this5.listCarrito = [];
                    _context2.next = 23;
                    break;
    
                  case 20:
                    _context2.prev = 20;
                    _context2.t0 = _context2["catch"](5);
                    console.log(_context2.t0);
    
                  case 23:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, null, [[5, 20]]);
          }))();
        },
        onSelected: function onSelected(item) {
          this.selected = item.item;
          this.cliente_nombre = this.selected.cliente_nombre;
          this.cliente_telefono = this.selected.cliente_telefono;
          this.cliente_cedula = this.selected.cliente_cedula;
          this.cliente_direccion = this.selected.cliente_direccion;
        },
        getSuggestionValue: function getSuggestionValue(suggestion) {
          return suggestion.item.cliente_nombre;
        }
      }),
      computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_5__["mapState"])(["usuario", "inventario"])), {}, {
        filteredOptions: function filteredOptions() {
          var _this6 = this;
    
          return [{
            data: this.listCliente.filter(function (option) {
              return option.cliente_nombre.toLowerCase().indexOf(_this6.cliente_nombre.toLowerCase()) > -1;
            })
          }];
        }
      }),
      mounted: function mounted() {
        this.listEmpleado();
        this.loadOrden();
        this.listClienteService();
        this.getInventario(this.usuario.name);
      }
    });
    
    /***/ }),
    
    /***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css&":
    /*!*********************************************************************************************************************************************************************************************************************************************************************!*\
      !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css& ***!
      \*********************************************************************************************************************************************************************************************************************************************************************/
    /*! no static exports found */
    /***/ (function(module, exports, __webpack_require__) {
    
    exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
    // imports
    
    
    // module
    exports.push([module.i, "\n.autosuggest-container input {\r\n  width: 100%;\r\n  /*padding: 0.5rem;*/\n}\n.autosuggest-container ul {\r\n  width: 100%;\r\n  color: rgba(30, 39, 46, 1);\r\n  list-style: none;\r\n  margin: 0;\r\n  padding: 0.5rem 0 0.5rem 0;\n}\n.autosuggest-container li {\r\n  margin: 0 0 0 0;\r\n  border-radius: 5px;\r\n  padding: 0.75rem 0 0.75rem 0.75rem;\r\n  display: flex;\r\n  align-items: center;\n}\n.autosuggest-container li:hover {\r\n  cursor: pointer;\n}\n.autosuggest-container {\r\n  justify-content: center;\r\n  width: 280px;\n}\n#autosuggest {\r\n  width: 100%;\r\n  display: block;\n}\n.autosuggest__results-item--highlighted {\r\n  background-color: rgba(51, 217, 178, 0.2);\n}\n.autosuggest__results ul {\r\n  background-color: #fff !important;\r\n  border: 1px solid #000;\r\n  position: absolute;\r\n  z-index: 100;\r\n  border-radius: 5px;\r\n  width: 260px;\n}\n.autosuggest__results ul li {\r\n  position: relative;\n}\n.container.main-pos {\r\n  margin-top: 15px;\n}\n.container.main-pos {\r\n  max-width: 95%;\n}\n.pointer {\r\n  cursor: pointer;\n}\n#cart-totals p strong:first-child,\r\n#cart-totals p strong:last-child {\r\n  margin-right: 30%;\n}\n@media only screen and (min-width: 1024px) {\n#productModal .modal-dialog {\r\n    max-width: 800px;\n}\n}\r\n", ""]);
    
    // exports
    
    
    /***/ }),
    
    /***/ "./node_modules/moment/moment.js":
    /*!***************************************!*\
      !*** ./node_modules/moment/moment.js ***!
      \***************************************/
    /*! no static exports found */
    /***/ (function(module, exports) {
    
    throw new Error("Module build failed: Error: EPERM: operation not permitted, open 'C:\\xampp\\htdocs\\vue-restaurante\\node_modules\\moment\\moment.js'");
    
    /***/ }),
    
    /***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css&":
    /*!*************************************************************************************************************************************************************************************************************************************************************************************************!*\
      !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css& ***!
      \*************************************************************************************************************************************************************************************************************************************************************************************************/
    /*! no static exports found */
    /***/ (function(module, exports, __webpack_require__) {
    
    
    var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Facturar.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css&");
    
    if(typeof content === 'string') content = [[module.i, content, '']];
    
    var transform;
    var insertInto;
    
    
    
    var options = {"hmr":true}
    
    options.transform = transform
    options.insertInto = undefined;
    
    var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);
    
    if(content.locals) module.exports = content.locals;
    
    if(false) {}
    
    /***/ }),
    
    /***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Facturar.vue?vue&type=template&id=22551f19&":
    /*!******************************************************************************************************************************************************************************************************!*\
      !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Facturar.vue?vue&type=template&id=22551f19& ***!
      \******************************************************************************************************************************************************************************************************/
    /*! exports provided: render, staticRenderFns */
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
    var render = function() {
      var _vm = this
      var _h = _vm.$createElement
      var _c = _vm._self._c || _h
      return _c("div", { staticClass: "container main-pos" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6 order-md-2 mb-4" }, [
            _c(
              "h4",
              {
                staticClass:
                  "d-flex justify-content-between align-items-center mb-3"
              },
              [
                _c("span", { staticClass: "text-muted" }, [
                  _vm._v("Cuenta a Pagar")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "badge badge-secondary badge-pill" }, [
                  _vm._v(_vm._s(_vm.listCarrito.length))
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "ul",
              { staticClass: "list-group mb-3" },
              [
                _c("li", { staticClass: "list-group-item d-flex" }, [
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      { staticClass: "col-md-6 order-md-1 autosuggest-container" },
                      [
                        _c("span", [_vm._v("Cliente:")]),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("vue-autosuggest", {
                          attrs: {
                            suggestions: _vm.filteredOptions,
                            "get-suggestion-value": _vm.getSuggestionValue,
                            "input-props": {
                              id: "autosuggest__input",
                              placeholder: "Cliente"
                            }
                          },
                          on: { selected: _vm.onSelected },
                          scopedSlots: _vm._u([
                            {
                              key: "default",
                              fn: function(ref) {
                                var suggestion = ref.suggestion
                                return _c(
                                  "div",
                                  {
                                    staticStyle: {
                                      display: "flex",
                                      "align-items": "center"
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticStyle: {
                                          "{ display": "'flex', color: 'navyblue'}"
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(suggestion.item.cliente_nombre)
                                        )
                                      ]
                                    )
                                  ]
                                )
                              }
                            }
                          ]),
                          model: {
                            value: _vm.cliente_nombre,
                            callback: function($$v) {
                              _vm.cliente_nombre = $$v
                            },
                            expression: "cliente_nombre"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-3 order-md-2" }, [
                      _c("span", [_vm._v("Cédula:")]),
                      _vm._v(" "),
                      _c("br"),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.cliente_cedula,
                            expression: "cliente_cedula"
                          }
                        ],
                        attrs: {
                          type: "text",
                          id: "cliente_cedula",
                          name: "cliente_cedula",
                          value: "Cédula",
                          placeholder: "Cedula",
                          size: "9"
                        },
                        domProps: { value: _vm.cliente_cedula },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.cliente_cedula = $event.target.value
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-3 order-md-3" }, [
                      _c("span", [_vm._v("Teléfono:")]),
                      _vm._v(" "),
                      _c("br"),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.cliente_telefono,
                            expression: "cliente_telefono"
                          }
                        ],
                        attrs: {
                          type: "text",
                          id: "cliente_telefono",
                          name: "cliente_telefono",
                          value: "Teléfono",
                          placeholder: "Teléfono",
                          size: "8"
                        },
                        domProps: { value: _vm.cliente_telefono },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.cliente_telefono = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass:
                      "list-group-item d-flex justify-content-between lh-condensed"
                  },
                  [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("span", { staticClass: "text-muted" }, [
                      _vm._v(_vm._s(_vm.listCarrito.lentgh))
                    ])
                  ]
                ),
                _vm._v(" "),
                _vm._l(_vm.listCarrito, function(itemcart, i) {
                  return _c(
                    "li",
                    {
                      key: i,
                      staticClass:
                        "list-group-item d-flex justify-content-between lh-condensed"
                    },
                    [
                      _c("div", { staticClass: "col-md-6" }, [
                        _c("h6", { staticClass: "my-0" }, [
                          _vm._v(_vm._s(itemcart.nombre))
                        ]),
                        _vm._v(" "),
                        _c("small", { staticClass: "text-muted" }, [
                          _vm._v(_vm._s(itemcart.categoria))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-2" }, [
                        _c("input", {
                          attrs: {
                            type: "text",
                            id: "precio_" + i,
                            name: "precio" + i,
                            placeholder: "0.00",
                            size: "4"
                          },
                          on: {
                            change: function($event) {
                              return _vm.changePrice(i)
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-2" }, [
                        _c("div", [
                          _c("span", { staticClass: "text-muted" }, [
                            _vm._v(_vm._s(itemcart.cant))
                          ])
                        ]),
                        _vm._v(" "),
                        _c("span", { staticClass: "text-muted" }, [
                          _vm._v(
                            _vm._s(
                              _vm.convertMoney(itemcart.cant * itemcart.precio)
                            )
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("b-icon-arrow-left-circle-fill", {
                        staticClass: "pointer my-auto",
                        attrs: { scale: "1.3" },
                        on: {
                          click: function($event) {
                            return _vm.deleteItem(i)
                          }
                        }
                      })
                    ],
                    1
                  )
                }),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "list-group-item d-flex justify-content-between",
                    attrs: { id: "cart-totals" }
                  },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("p", { staticClass: "d-flex justify-content-between" }, [
                          _c("span", [_vm._v("Sub Total")]),
                          _vm._v(" "),
                          _c("strong", [_vm._v(_vm._s(_vm.onViewSubTotal()))])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("p", { staticClass: "d-flex justify-content-between" }, [
                          _c("span", [_vm._v("Impuestos Servicio")]),
                          _vm._v(" "),
                          _c("strong", [_vm._v(_vm._s(_vm.onViewImp()))]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.checkImpuestos,
                                expression: "checkImpuestos"
                              }
                            ],
                            attrs: { type: "checkbox", id: "checkbox" },
                            domProps: {
                              checked: Array.isArray(_vm.checkImpuestos)
                                ? _vm._i(_vm.checkImpuestos, null) > -1
                                : _vm.checkImpuestos
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.checkImpuestos,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.checkImpuestos = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.checkImpuestos = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.checkImpuestos = $$c
                                }
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("p", { staticClass: "d-flex justify-content-between" }, [
                          _c("span", [_vm._v("Total")]),
                          _vm._v(" "),
                          _c("strong", [_vm._v(_vm._s(_vm.onViewTotal()))])
                        ])
                      ])
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "list-group-item d-flex justify-content-between" },
                  [
                    _c("span", [
                      _c("b", [_vm._v("Mesero:")]),
                      _vm._v(
                        "\n            " + _vm._s(this.empleado) + "\n          "
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "list-group-item d-flex justify-content-between" },
                  [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-danger btn-md",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.removeAllOrder()
                          }
                        }
                      },
                      [_vm._v("Quitar todo")]
                    )
                  ]
                )
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-success btn-lg btn-block",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.onSendOrder()
                  }
                }
              },
              [_vm._v("PROCESAR PEDIDO")]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6 order-md-1" }, [
            _c("h2", [_vm._v("Mesa " + _vm._s(this.SelectMesa))]),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c("h4", { staticClass: "mb-3" }, [_vm._v("Orden")]),
            _vm._v(" "),
            _c(
              "ul",
              { staticClass: "list-group mb-3" },
              [
                _c(
                  "li",
                  {
                    staticClass:
                      "list-group-item d-flex justify-content-between lh-condensed"
                  },
                  [
                    _vm._m(1),
                    _vm._v(" "),
                    _c("span", { staticClass: "text-muted" }, [
                      _vm._v(_vm._s(_vm.listOrden.lentgh))
                    ])
                  ]
                ),
                _vm._v(" "),
                _vm._l(_vm.listOrden, function(itemcart, i) {
                  return _c(
                    "li",
                    {
                      key: i,
                      staticClass:
                        "list-group-item d-flex justify-content-between lh-condensed"
                    },
                    [
                      _c("div", { staticClass: "col-md-8" }, [
                        _c("h6", { staticClass: "my-0" }, [
                          _vm._v(_vm._s(itemcart.nombre))
                        ]),
                        _vm._v(" "),
                        _c("small", { staticClass: "text-muted" }, [
                          _vm._v(_vm._s(itemcart.categoria))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c("div", [
                          _c("span", { staticClass: "text-muted" }, [
                            _vm._v(_vm._s(itemcart.cant))
                          ])
                        ]),
                        _vm._v(" "),
                        _c("span", { staticClass: "text-muted" }, [
                          _vm._v(
                            _vm._s(
                              _vm.convertMoney(itemcart.cant * itemcart.precio)
                            )
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("b-icon-arrow-right-circle-fill", {
                        staticClass: "pointer my-auto",
                        attrs: { scale: "1.3" },
                        on: {
                          click: function($event) {
                            return _vm.addCart(i)
                          }
                        }
                      })
                    ],
                    1
                  )
                }),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "list-group-item d-flex justify-content-between" },
                  [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-info btn-md",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.addAllOrder()
                          }
                        }
                      },
                      [_vm._v("Agregar todo")]
                    )
                  ]
                )
              ],
              2
            )
          ])
        ])
      ])
    }
    var staticRenderFns = [
      function() {
        var _vm = this
        var _h = _vm.$createElement
        var _c = _vm._self._c || _h
        return _c("div", [
          _c("h6", { staticClass: "my-0" }, [_vm._v("Producto")]),
          _vm._v(" "),
          _c("small", { staticClass: "text-muted" })
        ])
      },
      function() {
        var _vm = this
        var _h = _vm.$createElement
        var _c = _vm._self._c || _h
        return _c("div", [
          _c("h6", { staticClass: "my-0" }, [_vm._v("Producto")]),
          _vm._v(" "),
          _c("small", { staticClass: "text-muted" })
        ])
      }
    ]
    render._withStripped = true
    
    
    
    /***/ }),
    
    /***/ "./resources/js/views/Facturar.vue":
    /*!*****************************************!*\
      !*** ./resources/js/views/Facturar.vue ***!
      \*****************************************/
    /*! exports provided: default */
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    /* harmony import */ var _Facturar_vue_vue_type_template_id_22551f19___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Facturar.vue?vue&type=template&id=22551f19& */ "./resources/js/views/Facturar.vue?vue&type=template&id=22551f19&");
    /* harmony import */ var _Facturar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Facturar.vue?vue&type=script&lang=js& */ "./resources/js/views/Facturar.vue?vue&type=script&lang=js&");
    /* empty/unused harmony star reexport *//* harmony import */ var _Facturar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Facturar.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css&");
    /* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
    
    
    
    
    
    
    /* normalize component */
    
    var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
      _Facturar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
      _Facturar_vue_vue_type_template_id_22551f19___WEBPACK_IMPORTED_MODULE_0__["render"],
      _Facturar_vue_vue_type_template_id_22551f19___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
      false,
      null,
      null,
      null
      
    )
    
    /* hot reload */
    if (false) { var api; }
    component.options.__file = "resources/js/views/Facturar.vue"
    /* harmony default export */ __webpack_exports__["default"] = (component.exports);
    
    /***/ }),
    
    /***/ "./resources/js/views/Facturar.vue?vue&type=script&lang=js&":
    /*!******************************************************************!*\
      !*** ./resources/js/views/Facturar.vue?vue&type=script&lang=js& ***!
      \******************************************************************/
    /*! exports provided: default */
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    /* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Facturar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Facturar.vue?vue&type=script&lang=js&");
    /* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 
    
    /***/ }),
    
    /***/ "./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css&":
    /*!**************************************************************************!*\
      !*** ./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css& ***!
      \**************************************************************************/
    /*! no static exports found */
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    /* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Facturar.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Facturar.vue?vue&type=style&index=0&lang=css&");
    /* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
     /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 
    
    /***/ }),
    
    /***/ "./resources/js/views/Facturar.vue?vue&type=template&id=22551f19&":
    /*!************************************************************************!*\
      !*** ./resources/js/views/Facturar.vue?vue&type=template&id=22551f19& ***!
      \************************************************************************/
    /*! exports provided: render, staticRenderFns */
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    /* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_template_id_22551f19___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Facturar.vue?vue&type=template&id=22551f19& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Facturar.vue?vue&type=template&id=22551f19&");
    /* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_template_id_22551f19___WEBPACK_IMPORTED_MODULE_0__["render"]; });
    
    /* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Facturar_vue_vue_type_template_id_22551f19___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });
    
    
    
    /***/ })
    
    }]);