(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Ventas.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Ventas.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Ventas",
  data: function data() {
    return {
      listCuenta: [],
      currentSort: "cuenta_id",
      currentSortDir: "asc",
      pageSize: 6,
      currentPage: 1
    };
  },
  mounted: function mounted() {
    this.listCuentaService();
  },
  methods: {
    getType: function getType(value) {
      if (value == 0) {
        return "Express";
      } else {
        return "Mesa " + value;
      }
    },
    nextPage: function nextPage() {
      if (this.currentPage * this.pageSize < this.listCuenta.length) this.currentPage++;
    },
    prevPage: function prevPage() {
      if (this.currentPage > 1) this.currentPage--;
    },
    sort: function sort(s) {
      if (s === this.currentSort) {
        this.currentSortDir = this.currentSortDir === "asc" ? "desc" : "asc";
      }

      this.currentSort = s;
    },
    convertMoney: function convertMoney(value) {
      var formatterPeso = new Intl.NumberFormat("es-CR", {
        style: "currency",
        currency: "CRC",
        minimumFractionDigits: 0
      });
      var valueFinal = formatterPeso.format(value);
      return valueFinal;
    },
    listCuentaService: function listCuentaService() {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("ventas/list").then(function (response) {
        _this.listCuenta = response.data;
      })["catch"](function (error) {
        alert(error);
      });
    },
    imprimir: function imprimir(i) {
      var data = this.listCuenta.find(function (element) {
        return element.cuenta_id == i;
      });
      var listCarrito = [];
      var formData = new FormData();
      data.pedidos.forEach(function (item) {
        var itemcar = {
          nombre: item.prod_name,
          cant: item.detalle_cantidad,
          precio: item.prod_price
        };
        listCarrito.push(itemcar);
      });
      formData.append("pedidos", JSON.stringify(listCarrito));
      formData.append("mesa", data.cuenta_mesa);
      formData.append("cuenta", data.cuenta_id);
      formData.append("tipo", data.cuenta_tipo);
      formData.append("empleado", data.cuenta_empleado);
      formData.append("cedula", data.cliente_cedula);
      formData.append("cliente", data.cliente_nombre);
      formData.append("telefono", data.cliente_telefono);
      formData.append("subtotal", data.cuenta_subtotal);
      formData.append("impuesto", data.cuenta_impuesto);
      formData.append("total", data.cuenta_total);
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("ventas/print", formData).then(function (response) {})["catch"](function (error) {
        console.log(error);
      });
    }
  },
  computed: {
    getTotal: function getTotal() {
      var total = 0;
      this.listCuenta.map(function (data) {
        total = total + data.cuenta_total;
      });
      return this.convertMoney(total);
    },
    getTotalMesas: function getTotalMesas() {
      var total = 0;
      this.listCuenta.map(function (data) {
        if (data.cuenta_tipo === 0) {
          total = total + data.cuenta_total;
        }
      });
      return this.convertMoney(total);
    },
    getTotalExpress: function getTotalExpress() {
      var total = 0;
      this.listCuenta.map(function (data) {
        if (data.cuenta_tipo === 1) {
          total = total + data.cuenta_total;
        }
      });
      return this.convertMoney(total);
    },
    filteredEntries: function filteredEntries() {
      var _this2 = this;

      return this.listCuenta.sort(function (a, b) {
        var modifier = 1;
        if (_this2.currentSortDir === "desc") modifier = -1;
        if (a[_this2.currentSort] < b[_this2.currentSort]) return -1 * modifier;
        if (a[_this2.currentSort] > b[_this2.currentSort]) return 1 * modifier;
        return 0;
      }).filter(function (row, index) {
        var start = (_this2.currentPage - 1) * _this2.pageSize;
        var end = _this2.currentPage * _this2.pageSize;
        if (index >= start && index < end) return true;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Ventas.vue?vue&type=template&id=166d6e06&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Ventas.vue?vue&type=template&id=166d6e06& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("b-container", [
    _c("br"),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md order-md-1" },
        [
          _c("h4", { staticClass: "mb-4" }, [
            _vm._v("Cierre de Caja - Ventas de hoy")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md" }, [
              _c("p", [
                _c("b", [_vm._v("Monto Total:")]),
                _vm._v("\n            " + _vm._s(_vm.getTotal) + "\n          ")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md" }, [
              _c("p", [
                _c("b", [_vm._v("Monto Total Restaurante:")]),
                _vm._v(
                  "\n            " + _vm._s(_vm.getTotalMesas) + "\n          "
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md" }, [
              _c("p", [
                _c("b", [_vm._v("Monto Total Express:")]),
                _vm._v(
                  "\n            " +
                    _vm._s(_vm.getTotalExpress) +
                    "\n          "
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("table", { staticClass: "table t mt-4" }, [
            _c("thead", [
              _c("tr", [
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("cuenta_id")
                      }
                    }
                  },
                  [_vm._v("#")]
                ),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("cuenta_fecha")
                      }
                    }
                  },
                  [_vm._v("Orden")]
                ),
                _vm._v(" "),
                _c(
                  "th",
                  {
                    staticClass: "headOrderBy",
                    attrs: { scope: "col" },
                    on: {
                      click: function($event) {
                        return _vm.sort("empleado_nombre")
                      }
                    }
                  },
                  [_vm._v("Vendedor")]
                ),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Cantidad")]),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Sub Total")]),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Impuesto")]),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Total")]),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } }, [_vm._v("Acciones")]),
                _vm._v(" "),
                _c("th", { attrs: { scope: "col" } })
              ])
            ]),
            _vm._v(" "),
            _c(
              "tbody",
              [
                _vm._l(_vm.filteredEntries, function(data) {
                  return [
                    _c("tr", { key: data.cuenta_id }, [
                      _c("td", [_vm._v(_vm._s(data.cuenta_id))]),
                      _vm._v(" "),
                      _c("td", [
                        _c("b", [
                          _vm._v(_vm._s(_vm.getType(data.cuenta_mesa)))
                        ]),
                        _vm._v(
                          "\n                " +
                            _vm._s(data.cuenta_fecha) +
                            "\n              "
                        )
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(data.empleado_nombre))]),
                      _vm._v(" "),
                      _c("th", { attrs: { scope: "col" } }),
                      _vm._v(" "),
                      _c("td", [
                        _vm._v(_vm._s(_vm.convertMoney(data.cuenta_subtotal)))
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _vm._v(_vm._s(_vm.convertMoney(data.cuenta_impuesto)))
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _vm._v(_vm._s(_vm.convertMoney(data.cuenta_total)))
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-light btn-sm",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                data.show = !data.show
                              }
                            }
                          },
                          [_vm._v("Detalles")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-light btn-sm",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                return _vm.imprimir(data.cuenta_id)
                              }
                            }
                          },
                          [_vm._v("Imprimir")]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _vm._l(data.pedidos, function(pedido) {
                      return _c(
                        "tr",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: data.show,
                              expression: "data.show"
                            }
                          ],
                          key: pedido.detalle_serial
                        },
                        [
                          _c("td"),
                          _vm._v(" "),
                          _c("td"),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(pedido.prod_name))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(pedido.detalle_cantidad))]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(_vm.convertMoney(pedido.detalle_valor))
                            )
                          ])
                        ]
                      )
                    })
                  ]
                })
              ],
              2
            )
          ]),
          _vm._v(" "),
          _c("b-pagination", {
            staticClass: "mt-4",
            attrs: {
              "total-rows": _vm.listCuenta.length,
              "per-page": _vm.pageSize,
              "first-number": "",
              "last-number": ""
            },
            model: {
              value: _vm.currentPage,
              callback: function($$v) {
                _vm.currentPage = $$v
              },
              expression: "currentPage"
            }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Ventas.vue":
/*!***************************************!*\
  !*** ./resources/js/views/Ventas.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Ventas_vue_vue_type_template_id_166d6e06___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Ventas.vue?vue&type=template&id=166d6e06& */ "./resources/js/views/Ventas.vue?vue&type=template&id=166d6e06&");
/* harmony import */ var _Ventas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Ventas.vue?vue&type=script&lang=js& */ "./resources/js/views/Ventas.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Ventas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Ventas_vue_vue_type_template_id_166d6e06___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Ventas_vue_vue_type_template_id_166d6e06___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Ventas.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Ventas.vue?vue&type=script&lang=js&":
/*!****************************************************************!*\
  !*** ./resources/js/views/Ventas.vue?vue&type=script&lang=js& ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Ventas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Ventas.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Ventas.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Ventas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Ventas.vue?vue&type=template&id=166d6e06&":
/*!**********************************************************************!*\
  !*** ./resources/js/views/Ventas.vue?vue&type=template&id=166d6e06& ***!
  \**********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ventas_vue_vue_type_template_id_166d6e06___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Ventas.vue?vue&type=template&id=166d6e06& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Ventas.vue?vue&type=template&id=166d6e06&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ventas_vue_vue_type_template_id_166d6e06___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ventas_vue_vue_type_template_id_166d6e06___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);