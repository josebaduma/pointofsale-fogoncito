<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix'=>'mesas'], function () {
    //Mesas
    Route::post('create', 'API\MesaController@create');
    Route::get('list', 'API\MesaController@list');
    Route::put('update', 'API\MesaController@edit');
    Route::delete('delete/{id}', 'API\MesaController@delete');
});

Route::group(['prefix'=>'llevar'], function () {
    //Mesas
    Route::post('create', 'API\LlevarController@create');
    Route::get('list', 'API\LlevarController@list');
    Route::put('update', 'API\LlevarController@edit');
    Route::delete('delete/{id}', 'API\LlevarController@delete');
});

Route::group(['prefix'=>'meseros'], function () {
    Route::get('list', 'API\MeseroController@list');
    Route::get('empleado-mesa/{mesa}', 'API\MeseroController@getEmpleadoMesa');
});

Route::group(['prefix'=>'cliente'], function () {
    //Clientes
    Route::post('create', 'API\ClienteController@create');
    Route::get('list', 'API\ClienteController@list');
    Route::put('update', 'API\ClienteController@edit');
    Route::put('update/frecuente', 'API\ClienteController@edit_frecuente');
    Route::delete('delete/{id}', 'API\ClienteController@delete');
});

Route::group(['prefix'=>'category'], function () {
    //Categoria
    Route::post('create', 'API\CategoriaController@create');
    Route::get('list', 'API\CategoriaController@list');
    Route::put('update', 'API\CategoriaController@edit');
    Route::delete('delete/{id}', 'API\CategoriaController@delete');
});

Route::group(['prefix'=>'product'], function () {
    //Producto
    Route::post('create', 'API\ProductoController@create');
    Route::get('list', 'API\ProductoController@list');
    Route::get('list/{id}', 'API\ProductoController@visible');
    Route::put('update', 'API\ProductoController@edit');
    Route::delete('delete/{id}', 'API\ProductoController@delete');
});

Route::group(['prefix'=>'ventas'], function () {
    //Ventas
    Route::post('create', 'API\VentasController@create');
    Route::get('list', 'API\VentasController@list');
    Route::get('list/size/{date}', 'API\VentasController@get_size_list');
    Route::get('list/{date}', 'API\VentasController@list_params');
    Route::get('list/{date}/{page}', 'API\VentasController@list_param');
    Route::get('revenew/{date}', 'API\VentasController@get_revenue');
    Route::get('inventario/list', 'API\VentasController@get_inventario');
    Route::get('inventario/{date}', 'API\VentasController@list_productos');
    Route::put('inventario/update', 'API\VentasController@edit_inventario');
    Route::delete('inventario/delete/{id}', 'API\VentasController@delete_inventario');
    

    Route::post('print', 'API\VentasController@print_orden');
});

Route::group(['prefix'=>'items'], function () {
    // Items
    Route::post('create', 'API\ItemController@create');
    Route::post('create/item', 'API\ItemController@add_stock');
    Route::get('list', 'API\ItemController@list');
    Route::get('list/{id}', 'API\ItemController@list_item');
    Route::put('update', 'API\ItemController@edit');
    Route::put('update/item', 'API\ItemController@edit_stock');
    Route::delete('delete/{id}', 'API\ItemController@delete');
});

Route::group(['prefix'=>'orden'], function () {
    //Ordenes
    Route::get('load/{mesa}', 'API\OrdenController@load');
    Route::get('load/llevar/{mesa}', 'API\OrdenController@load_llevar');
    Route::get('load/express/{orden}', 'API\OrdenController@load_express');
    Route::get('list/express', 'API\OrdenController@list_express');
    
    Route::post('create', 'API\OrdenController@create');
    Route::post('create/llevar', 'API\OrdenController@create_llevar');
    Route::put('update', 'API\OrdenController@update');
    Route::put('change-mesa', 'API\OrdenController@change_mesa');

    Route::delete('delete/{orden_id}', 'API\OrdenController@delete');
    Route::delete('delete/llevar/{orden_id}', 'API\OrdenController@delete_llevar');
    Route::delete('delete-detalle/{serial}', 'API\OrdenController@delete_detalle');

    Route::post('print/{numOrden}/{mesa}/{tipo}', 'API\OrdenController@print');
    Route::post('print/detalle', 'API\OrdenController@print_registro');
});
