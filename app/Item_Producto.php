<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_Producto extends Model
{
    protected $table = "item_producto";

    protected $fillable = [
      'id',
      'item_id',
      'producto_id'
    ];

    public $timestamps = false;
}
