<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table = "cuentas";

    protected $fillable = [
      'cuenta_id',
      'cuenta_mesa',
      'cuenta_empleado',
      'cuenta_cliente',
      'cuenta_subtotal',
      'cuenta_impuesto',
      'cuenta_total',
      'cuenta_tipo',
      'cuenta_fecha'
    ];

    public $timestamps = false;
}
