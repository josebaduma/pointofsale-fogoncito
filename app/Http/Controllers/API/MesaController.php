<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mesa;

class MesaController extends Controller
{
    public function create(Request $request)
    {
        try {
            $mesaid = $request->input('mesaId');
            Mesa::insert([
                'mesas_id' => $mesaid,
                'mesas_sillas' => 4,
                'mesas_estado' => 0,
            ]);

            $response['success'] = true;
            $response['message'] = 'Se creó con exito';
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list()
    {
        try {
            $data = Mesa::get();

            $response['success'] = true;
            $response['mesas'] = $data;
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function edit(Request $request)
    {
        try {
            $idmesa = $request->input("idmesa");
            $estado = $request->input("estado");
    
            Mesa::where("mesas_id", $idmesa)->update([
                'mesas_estado' => $estado
            ]);
    
            $response['success'] = true;
            $response['message'] = "Actualizo exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
    public function delete($id)
    {
        try {
            Mesa::where("mesas_id", $id)->delete();

            $response['success'] = true;
            $response['message'] = "Eliminó exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
}
