<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Categoria;

class CategoriaController extends Controller
{
    public function create(Request $request)
    {
        try {
            $name = $request->input("name");
            $cocina = $request->input("cocina");

            Categoria::insert([
                'cat_nombre' => $name,
                'cat_cocina' => $cocina,
                'cat_delete' => 0,
                'cat_active' => 1,
            ]);

            $response['success'] = true;
            $response['message'] = 'Se creó con exito';
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list()
    {
        try {
            $data = Categoria::get();

            $response['success'] = true;
            $response['categorias'] = $data;
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function edit(Request $request)
    {
        try {
            $idcat = $request->input("idcat");
            $name = $request->input("name");
            $cocina = $request->input("cocina");
    
            Categoria::where("cat_id", $idcat)->update([
                'cat_nombre' => $name, 'cat_cocina' => $cocina
            ]);
    
            $response['success'] = true;
            $response['message'] = "Actualizo exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
    public function delete($id)
    {
        try {
            Categoria::where("cat_id", $id)->delete();

            $response['success'] = true;
            $response['message'] = "Eliminó exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo eliminar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
}
