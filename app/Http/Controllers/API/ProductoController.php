<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Producto;

class ProductoController extends Controller
{
    //
    public function create(Request $request)
    {
    
        $input['prod_name'] = $request->input('name');
        $input['prod_cod'] = $request->input('code');
        $input['prod_categoria'] = $request->input('categorie');
        $input['prod_price'] = $request->input('price');
        $input['prod_menu'] = $request->input('type');

        $input['prod_visible'] = 1;

        $data = Producto::insert($input);

        $response['message'] = "Guardo exitosamente ";
        $response['success'] = true;
        return $response;
    }

    public function edit(Request $request)
    {
        $idprod = $request->input("id");
        $code = $request->input("code");
        $name = $request->input("name");
        $price = $request->input("price");
        $category = $request->input("category");
        $type = $request->input("type");

        try {
            Producto::where("prod_id", $idprod)->update([
                'prod_name' => $name, 'prod_cod' => $code, 'prod_price' => $price, 'prod_categoria' => $category, 'prod_menu' => $type
            ]);

            $response['message'] = "Actualizo exitosamente";
            $response['success'] = true;
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list()
    {
        $data = Producto::join('categorias', 'categorias.cat_id', '=', 'productos.prod_categoria')
            ->orderBy('prod_name', 'asc')->get();

        return $data;
    }

    public function visible($menu)
    {
        $data = Producto::where("prod_menu", $menu)
            ->where("prod_visible", 1)
            ->join('categorias', 'categorias.cat_id', '=', 'productos.prod_categoria')
            ->where("cat_active", 1)
            ->orderBy('prod_name', 'asc')
            ->get();

        return $data;
    }

    public function delete($id)
    {
        $data = Producto::where("prod_id", $id)
            ->delete();
    }
}
