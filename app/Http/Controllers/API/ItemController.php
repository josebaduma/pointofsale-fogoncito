<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Items;
use App\Inventario;
use App\Item_Producto;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function create(Request $request)
    {
        try {
            $nombre = $request->input('nombre');
            $productos = $request->input('productos');

            $item = Items::create([
                'nombre' => $nombre,
            ]);

            foreach ($productos as $producto) {
                $data['item_id'] = $item->id;
                $data['producto_id'] = $producto;

                Item_Producto::create($data);
            }

            $response['success'] = true;
            $response['message'] = 'Se creó con exito';
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function add_stock(Request $request)
    {
        try {
            $data['item_id'] = $request->input("item_id");
            $data['fecha'] = $request->input("fecha");
            $data['entra'] = $request->input("entra");
            $data['sale'] = 0;

            $inventario = Inventario::create($data);

            $response['success'] = true;
            $response['id'] = $inventario->id;

            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo guardar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function edit_stock(Request $request)
    {
        try {
            $fecha = Carbon::instance(new \DateTime($request->input("fecha") . ' 06:00:00'));
            $fecha->setTimezone('America/Costa_Rica');

            $id = $request->input("id");
            $data['entra'] = $request->input("entra");
            $data['sale'] = $request->input("sale");

            $inventario = Inventario::where('id', '=', $id)
                ->whereDate('fecha', '=', $fecha)->update(['entra' => $data['entra'], 'sale' => $data['sale']]);

            $response['success'] = true;

            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo guardar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list_item($id)
    {
        try {
            $data = Items::join('inventario', 'inventario.item_id', '=', 'items.id')
                ->where('inventario.item_id', '=', $id)->get();

            $response['success'] = true;
            $response['items'] = $data;
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list()
    {
        try {
            $date =  Carbon::instance(new \DateTime('2022-07-10 06:00:00')); //Carbon::now()->locale('es_ES');
            $data = Items::join('inventario', 'inventario.item_id', '=', 'items.id')->join('item_producto', 'item_producto.item_id', '=', 'items.id')
            ->where('item_producto.producto_id', '=', 35)->whereDate('inventario.fecha', '=', $date->format('Y-m-d'));
            $list = $data->get();
            
            $data->increment('inventario.sale');
            

            $response['success'] = true;
            $response['items'] = $list;
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function edit(Request $request)
    {
        try {
            $id = $request->input("id");
            $nombre = $request->input("nombre");

            Items::where("id", $id)->update([
                'nombre' => $nombre
            ]);

            $response['success'] = true;
            $response['message'] = "Actualizo exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
    public function delete($id)
    {
        try {
            Items::where("id", $id)->delete();

            $response['success'] = true;
            $response['message'] = "Eliminó exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
}
