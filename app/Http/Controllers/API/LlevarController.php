<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Llevar;

class LlevarController extends Controller
{
    public function create(Request $request)
    {
        try {
            $mesaid = $request->input('mesaId');
            Llevar::insert([
                'llevar_id' => $mesaid,
                'llevar_estado' => 0,
            ]);

            $response['success'] = true;
            $response['message'] = 'Se creó con exito';
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list()
    {
        try {
            $data = Llevar::get();

            $response['success'] = true;
            $response['llevar'] = $data;
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function edit(Request $request)
    {
        try {
            $idmesa = $request->input("idmesa");
            $nombre = $request->input("nombre");
            $estado = $request->input("estado");
    
            Llevar::where("llevar_id", $idmesa)->update([
                'llevar_nombre' => $nombre,
                'llevar_estado' => $estado
            ]);
    
            $response['success'] = true;
            $response['message'] = "Actualizo exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
    public function delete($id)
    {
        try {
            Llevar::where("llevar_id", $id)->delete();

            $response['success'] = true;
            $response['message'] = "Eliminó exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
}
