<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mesero;

class MeseroController extends Controller
{
    public function list()
    {
        $data = Mesero::where("empleado_delete", 0)
        ->get();
      
        return $data;
    }

    public function getEmpleadoMesa($id)
    {
        $data = Mesero::where("empleado_delete", 0)
        ->join("orden", "orden.ord_empleado", "=", "empleados.empleado_id")
        ->where("orden.ord_mesa", $id)
        ->where("orden.ord_estado", 0)
        ->get();
      
        return $data;
    }
}
