<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Helpers\ExcelGenerator;
use Maatwebsite\Excel\Facades\Excel;
use Throwable;

class ExcelController extends Controller
{
    public function generateExcel($date)
    {
        try {
            $export = new ExcelGenerator();
            $export->setDate($date);
            return Excel::download($export, 'datos.xlsx');
        }catch (Throwable $th) {
            $response['success'] = false;
            $response['message'] = "Error";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
}
