<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Cliente;

class ClienteController extends Controller
{
    public function create(Request $request)
    {
        try {
            $nombre = $request->input('nombre');
            $cedula = $request->input('cedula');
            $empresa = $request->input('empresa');
            $telefono = $request->input('telefono');
            $direccion = $request->input('direccion');
            $turismo = $request->input('turismo');

            $lastClient = Cliente::create([
                'cliente_nombre' => $nombre,
                'cliente_cedula' => $cedula,
                'cliente_empresa' => $empresa,
                'cliente_telefono' => $telefono,
                'cliente_direccion' => $direccion,
                'cliente_turismo' => $turismo,
            ]);

            $response['success'] = true;
            $response['clientes'] = Cliente::all()->last();
            $response['message'] = 'Se creó con exito';
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list()
    {
        try {
            $data = Cliente::get();

            $response['success'] = true;
            $response['clientes'] = $data;
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function edit(Request $request)
    {
        try {
            $id = $request->input("id");
            $nombre = $request->input('nombre');
            $cedula = $request->input('cedula');
            $empresa = $request->input('empresa');
            $telefono = $request->input('telefono');
            $direccion = $request->input('direccion');
            $frecuente = $request->input('frecuente');
            $turismo = $request->input('turismo');

    
            Cliente::where("cliente_id", $id)->update([
                'cliente_nombre' => $nombre, 'cliente_cedula' => $cedula, 'cliente_empresa' => $empresa, 'cliente_telefono' => $telefono, 'cliente_direccion' => $direccion, 'cliente_turismo' => $turismo
            ]);
    
            $response['success'] = true;
            $response['message'] = "Actualizo exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function edit_frecuente(Request $request)
    {
        try {
            $id = $request->input("id");
            $frecuente = $request->input('frecuente');

    
            Cliente::where("cliente_id", $id)->update([
                'cliente_frecuente' => $frecuente
            ]);
    
            $response['success'] = true;
            $response['message'] = "Actualizo exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
    public function delete($id)
    {
        try {
            Cliente::where("cliente_id", $id)->delete();

            $response['success'] = true;
            $response['message'] = "Eliminó exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }
}
