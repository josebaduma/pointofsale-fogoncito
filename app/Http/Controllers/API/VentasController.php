<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;
use DataTime;

use App\Venta;
use App\Detalle;
use App\Items;
use App\Inventario;
use App\Item_Producto;
use App\Pedido;
use Carbon\Carbon;

class VentasController extends Controller
{
    public function create(Request $request)
    {
        try {
            $detalle = $request->input("pedidos");

            $cuentadata['cuenta_mesa'] = $request->input("mesa");
            $cuentadata['cuenta_empleado'] = $request->input("empleado");
            $cuentadata['cuenta_cliente'] = $request->input("cliente_id");
            $cuentadata['cuenta_subtotal'] = $request->input("subtotal");
            $cuentadata['cuenta_impuesto'] = $request->input("impuesto");
            $cuentadata['cuenta_total'] = $request->input("total");
            $cuentadata['cuenta_tipo'] = $request->input("tipo");
            // guarda el detalle
            $cuenta = Venta::create($cuentadata);

            $date = Carbon::now()->locale('es_ES');

            foreach (json_decode($detalle) as $item) {
                $itemped['detalle_producto_id'] = $item->id;
                $itemped['detalle_cuenta'] = $cuenta->id;
                $itemped['detalle_cantidad'] = $item->cant;
                $itemped['detalle_valor'] = $item->precio;

                $product = Pedido::where('detalle_orden_serial', '=', $item->serial)->get();
                $stock = $product[0]->detalle_orden_cantidad;

                Detalle::create($itemped);

                if($stock == $item->cant) {
                    Pedido::where('detalle_orden_serial', '=', $item->serial)->delete();
                } else {
                    Pedido::where('detalle_orden_serial', '=', $item->serial)
                    ->update(['detalle_orden_cantidad' => ($stock - $item->cant), ]);
                }

                $itemsB = Items::join('inventario', 'inventario.item_id', '=', 'items.id')->join('item_producto', 'item_producto.item_id', '=', 'items.id')
                    ->where('item_producto.producto_id', '=', $itemped['detalle_producto_id'])->whereDate('inventario.fecha', '=', $date->format('Y-m-d'));
                $itemsCount = Items::join('inventario', 'inventario.item_id', '=', 'items.id')->join('item_producto', 'item_producto.item_id', '=', 'items.id')
                    ->where('item_producto.producto_id', '=', $itemped['detalle_producto_id'])->whereDate('inventario.fecha', '=', $date->format('Y-m-d'))->count();
                $other = Item_Producto::where('producto_id', $itemped['detalle_producto_id'])->get();

                if ($itemsCount != 0) {
                    $itemsB->increment('inventario.sale', $itemped['detalle_cantidad']);
                } else if ($itemsCount == 0 && !empty($other[0])) {
                    //if(!empty($other)){
                    $data['item_id'] = $other[0]->item_id;
                    //$data['fecha'] = $date->format('Y-m-d');
                    $data['entra'] = 0;
                    $data['sale'] = $itemped['detalle_cantidad'];

                    $inventario = Inventario::create($data);
                    //}
                }
            }

            $this->print_factura($request, $cuenta->id);

            $response['success'] = true;
            $response['cuenta_id'] = $cuenta->id;

            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo guardar la venta";
            $response['error'] = $th->getMessage();
            return response()->json(mb_convert_encoding($response, "UTF-8", "UTF-8"));
        }
    }

    public function list()
    {
        $date = Carbon::now()->locale('es_ES');
        $date->setTimezone('America/Costa_Rica');
        //echo $fecha_hoy;
        $cuentas = Venta::join('empleados', 'empleados.empleado_id', '=', 'cuentas.cuenta_empleado')
            ->join('clientes', 'clientes.cliente_id', '=', 'cuentas.cuenta_cliente')
            ->where('clientes.cliente_nombre', '=', 'DUKE')
            ->whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))
            ->get();

        foreach ($cuentas as $value) {
            $value['show'] = false;
            $value['pedidos'] = Detalle::join('productos', 'productos.prod_id', '=', 'detalle.detalle_producto_id')
                ->where("detalle_cuenta", $value->cuenta_id)->get();
        }
        return $cuentas;
    }

    public function get_inventario()
    {
        $items = Items::join('inventario', 'inventario.item_id', '=', 'items.id', 'left outer')
            ->groupBy('items.id')
            ->select('items.id', 'items.nombre', Items::raw('SUM(inventario.entra) AS entra'), Items::raw('SUM(inventario.sale) AS sale'))
            ->get();

        foreach ($items as $value) {
            $value['productos'] = Item_Producto::where("item_id", $value->id)->pluck('producto_id')->toArray();
        }

        $response['inventario'] = $items;

        return response()->json($response, 200);
    }

    public function edit_inventario(Request $request)
    {
        $id = $request->input("id");
        $name = $request->input("nombre");
        $productos = $request->input("productos");

        try {
            Items::where("id", $id)->update([
                'nombre' => $name,
            ]);

            Item_Producto::where("item_id", $id)->delete();
            foreach ($productos as $producto) {
                $data['item_id'] = $id;
                $data['producto_id'] = $producto;

                Item_Producto::create($data);
            }

            $response['message'] = "Actualizo exitosamente";
            $response['success'] = true;
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function delete_inventario($id)
    {
        try {
            Items::where("id", $id)->delete();

            $response['success'] = true;
            $response['message'] = "Eliminó exitosamente";
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo actualizar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list_productos($date)
    {
        $date = Carbon::instance(new \DateTime($date . ' 06:00:00')); //Carbon::now()->locale('es_ES');
        $date->setTimezone('America/Costa_Rica');

        $cuentas = Venta::join('detalle', 'detalle.detalle_cuenta', '=', 'cuentas.cuenta_id')
            ->join('productos', 'productos.prod_id', '=', 'detalle.detalle_producto_id')
            ->whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))
            ->groupBy("productos.prod_name")->orderBy("productos.prod_name")
            ->select("productos.prod_name", Venta::raw('SUM(detalle.detalle_cantidad) AS cantidad'))->get();

        $response['cantidades'] = $cuentas;

        return response()->json($response, 200);
    }

    public function get_size_list($date)
    {
        $size = 0;
        $date = Carbon::instance(new \DateTime($date . ' 06:00:00'));
        $date->setTimezone('America/Costa_Rica');

        $cuentas = Venta::whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))->get();
        $size = $cuentas->count();

        return $size;
    }

    public function list_param($date, $page)
    {
        $itemPerPage = 10;
        $offset = ($page - 1) * $itemPerPage;
        $date = Carbon::instance(new \DateTime($date . ' 06:00:00')); //Carbon::now()->locale('es_ES');
        $date->setTimezone('America/Costa_Rica');

        $cuentas = Venta::join('empleados', 'empleados.empleado_id', '=', 'cuentas.cuenta_empleado')
            ->join('clientes', 'clientes.cliente_id', '=', 'cuentas.cuenta_cliente')
            ->whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))
            //->where('cuentas.cuenta_cliente', '=', 382)
            ->orderBy('cuenta_fecha', 'asc')
            ->offset($offset)
            ->limit($itemPerPage)
            ->get();

        foreach ($cuentas as $value) {
            $value['show'] = false;
            $value['pedidos'] = Detalle::join('productos', 'productos.prod_id', '=', 'detalle.detalle_producto_id')
                ->where("detalle_cuenta", $value->cuenta_id)->get();
        }
        return $cuentas;
    }

    public function get_revenue($date)
    {
        $date = Carbon::instance(new \DateTime($date . ' 06:00:00')); //Carbon::now()->locale('es_ES');
        $date->setTimezone('America/Costa_Rica');

        $cuentas = Venta::whereDate('cuenta_fecha', '=', $date->format('Y-m-d'));
        $salon = Venta::whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))->where('cuenta_tipo', '=', 0);
        $llevar = Venta::whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))->where('cuenta_tipo', '=', 2);
        $express = Venta::where('cuenta_tipo', '=', 1)->whereDate('cuenta_fecha', '=', $date->format('Y-m-d'));

        $total = $cuentas->sum('cuenta_total');
        $subtotal = $cuentas->sum('cuenta_subtotal');
        $impuestos = $cuentas->sum('cuenta_impuesto');
        $totalSalon = $salon->sum('cuenta_total');
        $totalLlevar = $llevar->sum('cuenta_total');
        $totalExpress = $express->sum('cuenta_total');

        $response['total'] = $total;
        $response['subtotal'] = $subtotal;
        $response['impuestos'] = $impuestos;
        $response['salon'] = $totalSalon + $totalLlevar;
        $response['express'] = $totalExpress;

        return response()->json($response, 200);
    }

    public function print_factura(Request $request, $cuenta)
    {
        if ($request->input("tipo") == "1") {
            $nombreImpresora = "smb://Desktop-84R16UU/Express";
            //$nombreImpresora = "smb://Usuario:fogon123@Desktop-QR3N5E4/LR2000E";
            //$nombreImpresora = "smb://Aaron:fogoncito123@Desktop-JR6NARG/LR2000";
            //$nombreImpresora = "smb://Aaron:Scoob1lm@Desktop-JR6NARG/LR2000";
        } else {
            //$nombreImpresora = "smb://Desktop-I69OAG2/POS-80C";
            $nombreImpresora = "smb://Usuario:fogon123@Desktop-QR3N5E4/LR2000E";
        }
        $connector = new WindowsPrintConnector($nombreImpresora);
        $impresora = new Printer($connector);
        $impresora->setJustification(Printer::JUSTIFY_CENTER);
        $impresora->setTextSize(1, 1);
        $impresora->setEmphasis(true);
        $impresora->text("RESTAURANTE EL FOGONCITO\n");
        $impresora->text("602270504\n");
        $impresora->text("Alejandro León Villegas\n");
        $impresora->text("2441-3698\n");
        $impresora->text("Acogido al Régimen Simplificado\n");
        $impresora->setEmphasis(false);
        $impresora->feed(1);

        $date = Carbon::now()->locale('es_ES');
        $date->setTimezone('America/Costa_Rica');
        $impresora->text($date->format('Y-m-d g:i A') . "\n");
        if ($request->input("tipo") == "1") {
            $impresora->text("Envio: " . $request->input("tipo_envio") . "\n");
        }
        $impresora->feed(3);


        $impresora->setJustification(Printer::JUSTIFY_LEFT);
        // obetner un array de pedidos
        $pedido = $request->input("pedidos");
        $impresora->text("CLIENTE: " . $request->input("cliente") . "\n");
        $impresora->text("CÉDULA: " . $request->input("cedula") . "\n");
        $impresora->text("TELÉFONO: " . $request->input("telefono") . "\n");
        $impresora->text("DIRECCIÓN: " . $request->input("direccion") . "\n");
        $impresora->feed(2);

        $impresora->text("Número de Factura: " . $cuenta);
        $impresora->feed(1);
        $impresora->text("Método de pago: " . $request->input("metodo_pago"));
        $impresora->feed(2);

        $impresora->text("Cant  Articulo      Precio\n");
        $impresora->text("===================\n");
        $impresora->feed(1);
        foreach (json_decode($pedido) as $item) {
            $total_detalle = $item->cant * $item->precio;
            $string_data = $item->cant . "   " . $item->nombre . "      " . $total_detalle . "\n";
            $impresora->text($string_data);
        }

        $impresora->feed(2);
        $impresora->text("===================\n");
        $impresora->feed(2);

        $impresora->text("Subtotal: " . $request->input("subtotal") . "\n");
        $impresora->text("Imp Servicio: " . $request->input("impuesto") . "\n");
        $impresora->text("Total: " . $request->input("total") . "\n");
        $impresora->text("Cancela con: \n");
        $impresora->text("Vuelto: " . $request->input("vuelto") . "\n");
        $impresora->feed(1);

        $impresora->setTextSize(1, 1);
        $impresora->setEmphasis(true);
        $impresora->setUnderline(true);
        $impresora->text("* Articulo Gravado \n");
        $impresora->setUnderline(false);
        $impresora->setEmphasis(false);


        $impresora->text("Autorizado mediante resolución N°. 11-97 Gaceta");
        $impresora->text("N°. 171 del 5-9-1997, de la Dirección General de");
        $impresora->text("Tributación Directa");
        $impresora->text("Original: Cliente, Copia, Trámite y Contabilidad");

        $impresora->feed(5);
        $connector->write(chr(27) . chr(109));
        $impresora->close();
    }

    public function print_orden(Request $request)
    {
        if ($request->input("tipo") == "1") {
            $nombreImpresora = "smb://Desktop-84R16UU/Express";
            //$nombreImpresora = "smb://Usuario:fogon123@Desktop-QR3N5E4/LR2000E";
            //$nombreImpresora = "smb://Aaron:fogoncito123@Desktop-JR6NARG/LR2000";
            //$nombreImpresora = "smb://Aaron:Scoob1lm@Desktop-JR6NARG/LR2000";
        } else {
            //$nombreImpresora = "smb://Desktop-I69OAG2/POS-80C";
            $nombreImpresora = "smb://Usuario:fogon123@Desktop-QR3N5E4/LR2000E";
        }
        $connector = new WindowsPrintConnector($nombreImpresora);
        $impresora = new Printer($connector);
        $impresora->setJustification(Printer::JUSTIFY_CENTER);
        $impresora->setTextSize(1, 1);
        $impresora->setEmphasis(true);
        $impresora->text("RESTAURANTE EL FOGONCITO\n");
        $impresora->text("602270504\n");
        $impresora->text("Alejandro León Villegas\n");
        $impresora->text("2441-3698\n");
        $impresora->text("Acogido al Régimen Simplificado\n");
        $impresora->setEmphasis(false);
        $impresora->feed(1);

        $date = Carbon::now()->locale('es_ES');
        $date->setTimezone('America/Costa_Rica');
        $impresora->text($request->input("fecha") . "\n"); //$date->format('Y-m-d g:i A') ."\n");
        if ($request->input("tipo") == 1) {
            $impresora->text("Envio: " . $request->input("tipo_envio") . "\n");
        }
        $impresora->feed(3);


        $impresora->setJustification(Printer::JUSTIFY_LEFT);
        // obtener un array de pedidos
        $pedido = $request->input("pedidos");
        $impresora->text("CLIENTE: " . $request->input("cliente") . "\n");
        $impresora->text("CÉDULA: " . $request->input("cedula") . "\n");
        $impresora->text("TELÉFONO: " . $request->input("telefono") . "\n");
        $impresora->text("DIRECCIÓN: " . $request->input("direccion") . "\n");
        $impresora->feed(2);

        $impresora->text("Número de Factura: " . $request->input("cuenta") . "\n");
        $impresora->feed(1);
        $impresora->text("Método de pago: " . $request->input("metodo_pago"));
        $impresora->feed(2);

        $impresora->text("Cant  Articulo      Precio\n");
        $impresora->text("===================\n");
        $impresora->feed(1);
        foreach (json_decode($pedido) as $item) {
            $string_data = $item->cant . "   " . $item->nombre . "      " . $item->precio . "\n";
            $impresora->text($string_data);
        }

        $impresora->feed(2);
        $impresora->text("===================\n");
        $impresora->feed(2);

        $impresora->text("Subtotal: " . $request->input("subtotal") . "\n");
        $impresora->text("Imp Servicio: " . $request->input("impuesto") . "\n");
        $impresora->text("Total: " . $request->input("total") . "\n");
        $impresora->text("Cancela con: \n");
        $impresora->text("Vuelto: \n");
        $impresora->feed(1);

        $impresora->setTextSize(1, 1);
        $impresora->setEmphasis(true);
        $impresora->setUnderline(true);
        $impresora->text("* Articulo Gravado \n");
        $impresora->setUnderline(false);
        $impresora->setEmphasis(false);


        $impresora->text("Autorizado mediante resolución N°. 11-97 Gaceta\n");
        $impresora->text("N°. 171 del 5-9-1997, de la Dirección General de\n");
        $impresora->text("Tributación Directa\n");
        $impresora->text("Original: Cliente, Copia, Trámite y Contabilidad\n");

        $impresora->feed(5);
        $connector->write(chr(27) . chr(109));
        $impresora->close();
    }
}
