<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;
use DataTime;

// add model
use App\Orden;
use App\Pedido;
use App\Producto;
use App\Mesa;
use App\Cliente;
use App\Llevar;
use Carbon\Carbon;

class OrdenController extends Controller
{
    public function create(Request $request)
    {
        try {
            // obetner un array de pedidos
            $pedido = $request->input("pedidos");

            $orderdata['ord_estado'] = 0;
            $orderdata['ord_mesa']   = $request->input("mesa");
            $orderdata['ord_empleado'] = $request->input("empleado");
            $orderdata['ord_subtotal'] = $request->input("total");
            $orderdata['ord_total'] = $request->input("total");
            $orderdata['ord_tipo'] = $request->input("tipo");
            // guarda el pedido
            $order = Orden::create($orderdata);

            foreach (json_decode($pedido) as $item) {
                $itemped['detalle_orden_producto_id'] = $item->id;
                $itemped['detalle_orden_id']   = $order->id;
                $itemped['detalle_orden_cantidad'] = $item->cant;
                $itemped['detalle_orden_valor'] = $item->precio;
                $itemped['detalle_orden_descripcion'] = $item->descripcion;
                $itemped['detalle_orden_estado'] = $item->estado;

                Pedido::create($itemped);
            }

            Mesa::where('mesas_id', $request->input("mesa"))
                ->update(['mesas_estado' => 1]);

            $response['success'] = true;
            $response['message'] = "Guardo exitosamente";
            $response['orden_id'] =  $order->id;

            return response()->json($response);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function create_llevar(Request $request)
    {
        try {
            // obetner un array de pedidos
            $pedido = $request->input("pedidos");

            $orderdata['ord_estado'] = 0;
            $orderdata['ord_mesa']   = $request->input("llevar");
            $orderdata['ord_empleado'] = $request->input("empleado");
            $orderdata['ord_subtotal'] = $request->input("total");
            $orderdata['ord_total'] = $request->input("total");
            $orderdata['ord_tipo'] = $request->input("tipo");
            // guarda el pedido
            $order = Orden::create($orderdata);

            foreach (json_decode($pedido) as $item) {
                $itemped['detalle_orden_producto_id'] = $item->id;
                $itemped['detalle_orden_id']   = $order->id;
                $itemped['detalle_orden_cantidad'] = $item->cant;
                $itemped['detalle_orden_valor'] = $item->precio;
                $itemped['detalle_orden_descripcion'] = $item->descripcion;

                Pedido::create($itemped);
            }

            Llevar::where('llevar_id', $request->input("llevar"))
                ->update(['llevar_estado' => 1]);

            $response['success'] = true;
            $response['message'] = "Guardo exitosamente";
            $response['orden_id'] =  $order->id;

            return response()->json($response);
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function list_express()
    {

        // get order per today
        $order = Orden::join('empleados', 'empleados.empleado_id', '=', 'orden.ord_empleado')
            ->where("ord_tipo", '=', 1)->where("ord_estado", '=', 0)->get();

        foreach ($order as $value) {
            // add value show
            $value['show'] = false;
            // add day details for order
            $value['pedidos'] = Pedido::join('productos', 'productos.prod_id', '=', 'detalle_orden.detalle_orden_producto_id')
                ->where("detalle_orden_id", $value->ord_id)->get();
        }
        return $order;
    }

    public function load($mesa)
    {
        try {
            // get order per today
            $order = Orden::where("ord_estado", '=', 0)
                ->where('ord_tipo', '!=', 2)
                ->join('mesas', 'mesas.mesas_id', '=', 'orden.ord_mesa')
                ->where('mesas_estado', 1)
                ->where('mesas.mesas_id', $mesa)->get();

            foreach ($order as $value) {
                // add value show
                $value['show'] = false;
                // add day details for order
                $value['pedidos'] = Pedido::join('productos', 'productos.prod_id', '=', 'detalle_orden.detalle_orden_producto_id')
                    ->join('categorias', 'categorias.cat_id', '=', 'productos.prod_categoria')
                    ->where("detalle_orden_id", '=', $value->ord_id)->get();
            }
            return $order;
        } catch (\Throwable $th) {
            $response['success'] = false;
            $response['message'] = "No se pudo listar";
            $response['error'] = $th->getMessage();
            return response()->json($response, 400);
        }
    }

    public function load_llevar($mesa)
    {

        // get order per today
        $order = Orden::where("ord_estado", '=', 0)
            ->where('ord_tipo', 2)
            ->join('llevar', 'llevar.llevar_id', '=', 'orden.ord_mesa')
            ->where('llevar_estado', 1)
            ->where('llevar.llevar_id', $mesa)->get();

        foreach ($order as $value) {
            // add value show
            $value['show'] = false;
            // add day details for order
            $value['pedidos'] = Pedido::join('productos', 'productos.prod_id', '=', 'detalle_orden.detalle_orden_producto_id')
                ->join('categorias', 'categorias.cat_id', '=', 'productos.prod_categoria')
                ->where("detalle_orden_id", '=', $value->ord_id)->get();
        }
        return $order;
    }

    public function load_express($orden)
    {

        // get order per today
        $order = Orden::where("ord_estado", '=', 0)
            ->where('ord_id', $orden)
            ->where('ord_tipo', 1)->get();

        foreach ($order as $value) {
            // add value show
            $value['show'] = false;
            // add day details for order
            $value['pedidos'] = Pedido::join('productos', 'productos.prod_id', '=', 'detalle_orden.detalle_orden_producto_id')
                ->join('categorias', 'categorias.cat_id', '=', 'productos.prod_categoria')
                ->where("detalle_orden_id", '=', $value->ord_id)->get();
        }
        return $order;
    }

    public function update(Request $request)
    {
        Orden::where('ord_id', $request->input('orden_id'))
            ->update(['ord_subtotal' => $request->input('total'), 'ord_total' => $request->input('total'), 'ord_empleado' => $request->input('empleado')]);

        $pedido = $request->input("pedidos");
        foreach (json_decode($pedido) as $item) {
            $serialinfo =  $item->serial;
            $itemped['detalle_orden_producto_id'] = $item->id;
            $itemped['detalle_orden_id']   = $request->input('orden_id');
            $itemped['detalle_orden_cantidad'] = $item->cant;
            $itemped['detalle_orden_valor'] = $item->precio;
            $itemped['detalle_orden_descripcion'] = $item->descripcion;

            if ($serialinfo == 0) {
                Pedido::create($itemped);
            } else {
                Pedido::where('detalle_orden_serial', '=', $serialinfo)
                    ->update(['detalle_orden_cantidad' => $itemped['detalle_orden_cantidad'], 'detalle_orden_valor' => $itemped['detalle_orden_valor'], 'detalle_orden_descripcion' => $itemped['detalle_orden_descripcion']]);
            }
        }

        $response['success'] = true;
        $response['message'] = "Guardo exitosamente";
    }

    public function delete($orden_id)
    {
        $orden = Orden::where('ord_id', $orden_id)->get();
        Mesa::where('mesas_id', $orden[0]->ord_mesa)->update(
            ['mesas_estado' => 0]
        );

        Orden::where('ord_id', $orden_id)->delete();
        //Pedido::where('detalle_orden_id', $orden_id)->delete();
    }

    public function delete_llevar($orden_id)
    {
        $orden = Orden::where('ord_id', $orden_id)->get();
        Llevar::where('llevar_id', $orden[0]->ord_mesa)->update(
            ['llevar_nombre' => 'Cliente', 'llevar_estado' => 0]
        );

        Orden::where('ord_id', $orden_id)->delete();
        Pedido::where('detalle_orden_id', $orden_id)->delete();
    }

    public function delete_detalle($serial)
    {
        $data = Pedido::where('detalle_orden_serial', $serial)->delete();
        return $data;
    }

    public function change_mesa(Request $request)
    {
        $orden = $request->input('id');
        $mesaActual = $request->input('actual');
        $mesaDestino = $request->input('destino');

        Orden::where('ord_id', $orden)
            ->update(['ord_mesa' => $mesaDestino]);

        Mesa::where('mesas_id', $mesaActual)
            ->update(['mesas_estado' => 0]);

        Mesa::where('mesas_id', $mesaDestino)
            ->update(['mesas_estado' => 1]);

        return 'true';
    }

    public function print($numOrden, $mesa, $tipo)
    {
        $order = Producto::select('categorias.cat_cocina', 'productos.prod_name', 'detalle_orden.detalle_orden_cantidad', 'detalle_orden.detalle_orden_descripcion')
            ->join('categorias', 'categorias.cat_id', '=', 'productos.prod_categoria')
            ->join('detalle_orden', 'detalle_orden.detalle_orden_producto_id', '=', 'productos.prod_id')
            ->where("detalle_orden.detalle_orden_id", $numOrden)
            ->where("detalle_orden.detalle_orden_estado", 0)
            ->get();
        
        $empleado = Orden::select('empleados.empleado_nombre')
            ->join('empleados', 'empleados.empleado_id', '=', 'orden.ord_empleado')
            ->where("orden.ord_id", $numOrden)
            ->get();

        echo ("\n\n");
        $count = count($order);
        $cocinaA = [];
        $cocinaB = [];
        $cocinaC = [];
        $cocinaD = [];
        $cocinaE = [];
        $cocinaF = [];

        for ($i = 0; $i < $count; $i++) {
            if ($order[$i]['cat_cocina'] == 1) {
                array_push($cocinaA, $order[$i]);
            } elseif ($order[$i]['cat_cocina'] == 2) {
                array_push($cocinaB, $order[$i]);
            } elseif ($order[$i]['cat_cocina'] == 3) {
                array_push($cocinaC, $order[$i]);
            } elseif ($order[$i]['cat_cocina'] == 4) {
                array_push($cocinaD, $order[$i]);
            } elseif ($order[$i]['cat_cocina'] == 0) {
                array_push($cocinaE, $order[$i]);
            } elseif ($order[$i]['cat_cocina'] == 5) {
                array_push($cocinaF, $order[$i]);
            }
        }


        $date = Carbon::now()->locale('es_ES');
        $date->setTimezone('America/Costa_Rica');


        for ($i = 0; $i < 6; $i++) {
            $print_order = [];
            if ($i == 1) {
                $print_order = $cocinaA;
            } elseif ($i == 2) {
                $print_order = $cocinaB;
            } elseif ($i == 3) {
                $print_order = $cocinaC;
            } elseif ($i == 4) {
                $print_order = $cocinaD;
            } elseif ($i == 0) {
                $print_order = $cocinaE;
            } elseif ($i == 5) {
                $print_order = $cocinaF;
            }

            if (count($print_order) != 0) {
                $print_order = json_encode($print_order);

                if ($i == 0 || $i == 5) {
                    //$nombreImpresora = "smb://Desktop-I69OAG2/POS-80C";
                    $nombreImpresora = "smb://Usuario:fogon123@Desktop-QR3N5E4/LR2000E";
                    $connector = new WindowsPrintConnector($nombreImpresora);
                    $impresora = new Printer($connector);
                } else {
                    $nombreImpresora = "192.168.100.100";
                    //$nombreImpresora = "smb://Desktop-J521TTI/COCINA";
                    $connector = new NetworkPrintConnector($nombreImpresora, 9100);
                    //$connector = new WindowsPrintConnector($nombreImpresora);
                    $impresora = new Printer($connector);
                }

                try {
                    $impresora->setJustification(Printer::JUSTIFY_CENTER);
                    $impresora->setTextSize(2, 2);
                    if ($mesa == 0) {
                        $impresora->text("PARA LLEVAR\n");
                    } elseif ($tipo == 2) {
                        $impresora->text("PARA LLEVAR\n");
                    } else {
                        $impresora->text("Mesa: " . $mesa . "\n");
                    }
                    $impresora->text("Cocina: " . $i . "\n");
                    $impresora->feed(1);
                    $impresora->text($date->format('Y-m-d g:i A') . "\n\n");
                    $impresora->text($empleado[0]->empleado_nombre . "\n");
                    $impresora->feed(3);

                    $impresora->setTextSize(2, 2);
                    $impresora->text("Cantidad  Producto\n");
                    $impresora->text("==================\n");
                    $impresora->feed(1);

                    echo ($print_order);
                    foreach (json_decode($print_order) as $item) {
                        $string_data = str_pad($item->detalle_orden_cantidad, 3) . str_pad($item->prod_name, 10) . "\n";
                        //$item->detalle_orden_cantidad . "  " . $item->prod_name . "     " . "\n";
                        $impresora->text($string_data);
                        $string_data = $item->detalle_orden_descripcion . "\n\n\n";
                        $impresora->text($string_data);
                    }
                    $impresora->feed(2);
                    $impresora->text("==================");

                    $impresora->feed(5);
                    $connector->write(chr(27) . chr(109));
                } catch (\Throwable $th) {
                    //throw $th;
                } finally {
                    $impresora->close();
                }
            }
        }

        Pedido::where('detalle_orden_id', '=', $numOrden)
            ->update(['detalle_orden_estado' => 1]);

        return $order;
    }

    public function print_registro(Request $request)
    {
        $cantidad = $request->input("cantidad");
        $nombre = $request->input("nombre");
        $descripcion = $request->input("descripcion");
        $mesa = $request->input("mesa");
        $i = $request->input("cocina");

        $date = Carbon::now()->locale('es_ES');
        $date->setTimezone('America/Costa_Rica');

        if ($i == 0) {
            $nombreImpresora = "smb://Desktop-I69OAG2/POS-80C";
            $connector = new WindowsPrintConnector($nombreImpresora);
            $impresora = new Printer($connector);
        } else {
            $nombreImpresora = "192.168.100.100";
            $connector = new NetworkPrintConnector($nombreImpresora, 9100);
            $impresora = new Printer($connector);
        }

        try {
            $impresora->setJustification(Printer::JUSTIFY_CENTER);
            $impresora->setTextSize(2, 2);
            if ($mesa == 0) {
                $impresora->text("PARA LLEVAR\n");
            } else {
                $impresora->text("Mesa: " . $mesa . "\n");
            }
            $impresora->text("Cocina: " . $i . "\n");
            $impresora->feed(1);
            $impresora->text($date->format('Y-m-d g:i A') . "\n");
            $impresora->feed(3);

            $impresora->setTextSize(2, 2);
            $impresora->text("Cant  Articulo      Descripción\n");
            $impresora->text("==================\n");
            $impresora->feed(1);

            $string_data = $cantidad . "  " . $nombre . "     " . "\n";
            $impresora->text($string_data);
            $string_data = $descripcion . "\n";
            $impresora->text($string_data);
            $impresora->text("\n\n");

            $impresora->feed(2);
            $impresora->text("==================");

            $impresora->feed(5);
            $connector->write(chr(27) . chr(109));
        } catch (\Throwable $th) {
            //throw $th;
        } finally {
            $impresora->close();
        }
    }
}
