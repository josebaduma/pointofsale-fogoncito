<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesa extends Model
{
    protected $table = "mesas";

    protected $fillable = [
      'mesas_id',
      'mesas_sillas',
      'mesas_estado',
    ];

    public $timestamps = false;
}
