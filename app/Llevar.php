<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Llevar extends Model
{
    protected $table = "llevar";

    protected $fillable = [
      'llevar_id',
      'llevar_nombre',
      'llevar_estado',
    ];

    public $timestamps = false;
}
