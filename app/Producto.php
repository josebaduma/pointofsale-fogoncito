<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = "productos";

    protected $fillable = [
      'prod_id',
      'prod_cod',
      'prod_name',
      'prod_price',
      'prod_categoria',
      'prod_visible',
      'prod_menu',
    ];

    public $timestamps = false;
}
