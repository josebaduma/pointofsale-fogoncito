<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = "clientes";

    protected $fillable = [
      'cliente_id',
      'cliente_nombre',
      'cliente_cedula',
      'cliente_empresa',
      'cliente_telefono',
      'cliente_direccion',
      'cliente_frecuente'
    ];

    public $timestamps = false;
}
