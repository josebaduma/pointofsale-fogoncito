<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model
{
    protected $table = "detalle";

    protected $fillable = [
      'detalle_id',
      'detalle_producto_id',
      'detalle_cantidad',
      'detalle_cuenta',
      'detalle_valor',
      'detalle_descripcion'
    ];

    public $timestamps = false;
}
