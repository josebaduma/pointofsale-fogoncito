<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = "detalle_orden";

    protected $fillable = [
      'detalle_orden_serial',
      'detalle_orden_producto_id',
      'detalle_orden_cantidad',
      'detalle_orden_id',
      'detalle_orden_valor',
      'detalle_orden_descripcion',
      'detalle_orden_estado'
    ];

    public $timestamps = false;
}
