<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    protected $table = "orden";

    protected $fillable = [
      'ord_id',
      'ord_mesa',
      'ord_empleado',
      'ord_subtotal',
      'ord_impuesto',
      'ord_total',
      'ord_estado',
      'ord_fecha',
      'ord_tipo'
    ];

    public $timestamps = false;
}
