<?php

namespace App\Helpers;

use App\Detalle;
use App\Item_Producto;
use App\Items;
use App\Venta;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class ExcelGenerator implements FromCollection, WithMapping, WithEvents
{
    private $date_requested = '2023-05-01';

    public function setDate($value)
    {
        $this->date_requested = $value;
    }
    public function collection()
    {
        $date = Carbon::instance(new \DateTime($this->date_requested . ' 06:00:00'));
        $date->setTimezone('America/Costa_Rica');

        $cuentas = Venta::whereDate('cuenta_fecha', '=', $date->format('Y-m-d'));
        $cuentasSalon = Venta::whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))->where('cuenta_tipo', '=', 0);
        $cuentasLlevar = Venta::whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))->where('cuenta_tipo', '=', 2);
        $cuentasExpress = Venta::whereDate('cuenta_fecha', '=', $date->format('Y-m-d'))->where('cuenta_tipo', '=', 1);

        $total = $cuentas->sum('cuenta_total');
        $totalSalon = $cuentasSalon->sum('cuenta_total') + $cuentasLlevar->sum('cuenta_total');
        $totalExpress = $cuentasExpress->sum('cuenta_total');
        $subtotal = $cuentas->sum('cuenta_subtotal');
        $impuestos = $cuentas->sum('cuenta_impuesto');

        $data = [
            ["", "Total Vendido", "Total Salon", "Total Express", "Subtotal", "Impuesto 10%"],
            ["", $total, $totalSalon, $totalExpress, $subtotal, $impuestos],
            [""],
            [""],
            ["", "Nombre", "Cantidad vendida"]
        ];

        $items = Detalle::join('cuentas', 'cuentas.cuenta_id', '=', 'detalle.detalle_cuenta')
            ->join('productos', 'productos.prod_id', '=', 'detalle.detalle_producto_id')
            ->whereDate('cuentas.cuenta_fecha', '=', $date)
            ->groupBy('detalle.detalle_producto_id')
            ->select('productos.prod_name', Detalle::raw('SUM(detalle.detalle_cantidad) AS quantity'))
            ->get();

        foreach ($items as $value) {
            $data[] = ["", $value['prod_name'], $value['quantity']];
        }

        return collect($data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:XFD1048576')->applyFromArray([
                    'font' => [
                        'size' => 14,
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A1:Z1')->applyFromArray([
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                    ],
                ]);

                $event->sheet->getStyle('A5:Z5')->applyFromArray([
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
                $event->sheet->autoSize();
            },
        ];
    }

    public function map($data): array
    {
        return $data;
    }
}