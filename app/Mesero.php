<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesero extends Model
{
    protected $table = "empleados";

    protected $fillable = [
        'empleado_id',
        'empleado_nombre',
        'empleado_active',
        'empleado_delete'
    ];

    public $timestamps = false;
}
